/* File name: table.h
*  Compiler:  MS Visual Studio 2012
*  Author:	  Brodie Taillefer, 040 757 711
*  Assignment: Scanner Assignment #2
*  Date:      March, 5, 2015
*  Professor: Sv. Ranev
*  Purpose:   Header file table.h to create the members and data to deal with the transition table, includes preprocessor directives, the transition table, accepting state definitions, and 
*			  prototypes for the accepting actionfunctions.
*  Function List: No functions only function prototypes
*/

#ifndef  TABLE_H_
#define  TABLE_H_ 

#ifndef BUFFER_H_
#include "buffer.h"
#endif

#ifndef NULL
#include <_null.h> /* NULL pointer constant is defined there */
#endif

/*   Source end-of-file (SEOF) sentinel symbol */
#define SEOF 255
#define RUNETIME_ERROR 1
#define MAX_STR_ERROR 17
#define MIN_INT 0

#define ES  12 /* Error state */
#define IS -1    /* Inavalid state */

/* State transition table definition */
#define TABLE_COLUMNS 8

/*transition table - type of states defined in separate table */

int  st_table[][TABLE_COLUMNS] = {
		{ 1, 6, 4, 4, IS, IS, IS }, /*State 0*/
		{ 1, 1, 1, 1, 2, 3, 2 }, /* State 1*/
		{ IS, IS, IS, IS, IS, IS, IS}, /*State 2*/
		{ IS, IS, IS, IS, IS, IS, IS}, /*State 3*/
		{ ES, 4, 4, 4, 7, 5, 5 }, /*State 4*/
		{ IS, IS, IS, IS, IS, IS, IS}, /*State 5*/
		{ ES, 10, 9, ES, 7, ES, 5 }, /*State 6*/
		{ 8, 7, 7, 7, 8, 8, 8 }, /*State 7*/
		{ IS, IS, IS, IS, IS, IS, IS }, /*State 8*/
		{ ES, 9, 9, ES, ES, ES, 11 }, /* State 9*/
		{ ES, ES, ES, ES, ES, ES, 11 }, /*State 10*/
		{ IS, IS, IS, IS, IS, IS, IS }, /*State 11*/
		{ IS, IS, IS, IS, IS, IS, IS }, /*State 12*/
};

	/* Accepting state table definition */
#define ASWR     1  /* accepting state with retract */
#define ASNR     2  /* accepting state with no retract */
#define NOAS     0  /* not accepting state */

 int as_table[] = {NOAS, NOAS, ASWR, ASNR, NOAS, ASWR, NOAS, NOAS, ASWR, NOAS, NOAS, ASWR, ASNR };

/* Accepting action function declarations */
Token aa_func02(char *lexeme);
Token aa_func03(char *lexeme);
Token aa_func05(char *lexeme);
Token aa_func08(char *lexeme);
Token aa_func11(char *lexeme);
Token aa_func12(char *lexeme);


/* defining a new type: pointer to function (of one char * argument)
returning Token
*/
typedef Token(*PTR_AAF)(char *lexeme);


/* Accepting function (action) callback table (array) definition */
PTR_AAF aa_table[] = { NULL, NULL, aa_func02, aa_func03, NULL, aa_func05, NULL, NULL, aa_func08, NULL, NULL, aa_func11, aa_func12};

#define KWT_SIZE  8

	char * kw_table[] = {
		"ELSE",
		"IF",
		"INPUT",
		"OUTPUT",
		"PLATYPUS",
		"REPEAT",
		"THEN",
		"USING"
	};

#endif
