
KSP Version: 0.90.0

Installed Modules:

- ActiveTextureManagement-x86-Basic 4-3
- AmbientLightAdjustment 1.3.1.1
- AnimatedDecouplers-x86 1.0.1
- CoherentContracts 1.02
- CommunityResourcePack 0.3.2
^ ContractConfigurator 0.5.4
- ContractConfigurator-RemoteTech 1.0.2
- ContractConfigurator-SCANsat 1.0.2
- ContractsWindowPlus 3.1
- CrossFeedEnabler v3.2
- CrowdSourcedScience v1.4.0
- DeadlyReentry v6.4.0
- DistantObject v1.5.1
- DistantObject-default v1.5.1
- DMagicOrbitalScience 0.9.1
- DockingPortAlignmentIndicator 5.1
- EnvironmentalVisualEnhancements-Config-Default 7-4
- EnvironmentalVisualEnhancements-HR 7-4
- FASALaunchClamps 5.00
- FerramAerospaceResearch v0.14.6
- FirespitterCore 7.0.5463.30802
- FreedomTex 1.4
- Fusebox 1.2
- ImprovedChaseCamera 1.4.0
- KAS 0.4.10
- KerbalAlarmClock v3.2.2.0
- KerbalConstructionTime 1.1.2
- ModuleManager 2.5.8
- ModuleRCSFX v3.4
- NearFutureConstruction 0.4.0
- NearFuturePropulsion 0.4.0
- NearFutureSolar 0.4.0
- OrbitalMaterialScience 0.5.1
- PortableScienceContainer 1.2.1
- ProceduralDynamics v0.9.3
- ProceduralFairings v3.11
- ProceduralParts v1.0.0
- ProceduralParts-Textures-SaturnNova 1
- RealChute 1.2.6.3
- Regolith 0.1.2
- RemoteTech v1.6.0
- SCANsat 8.1
- ScienceAlert 1.8.4
- StageRecovery 1.5.3
- ToadicusTools 4
- Toolbar 1.7.8
- Trajectories 1.1.2
- TransferWindowPlanner v1.2.3.0
- USI-ART 0.6.1
- USI-EXP 0.3.1
- USITools 0.3.1
- VOID 0.16.4