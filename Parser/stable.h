#ifndef _STABLE_H
#define _STABLE_H

#include "buffer.h"
#define STATUS_FIELD_DEFAULT 0xFFF8 /* 1111 1111 1111 1000*/
#define STATUS_FIELD_FP 0x0002   /* 0000 0000 0000 0010*/
#define STATUS_FIELD_INT 0x0004  /* 0000 0000 0000 0100*/
#define STATUS_FIELD_STRING 0x0006 /* 0000 0000 0000 0110*/
#define STATUS_FIELD_UPDATE 0x0001 /* 0000 0000 0000 0001*/
#define STATUS_FIELD_RESET_UPDATE 0xFFF9 /* 1111 1111 1111 1001*/
#define RESET_FLAG 0  /* Reset value for our local r_flag */
#define DEFAULT_INT 0 /*Default int value*/
#define DEFAULT_FLOAT 0.0f /*Default float value*/
#define DEFAULT_STRING -1 /*Default string value*/
#define ERROR_SIZE 0 /*Symbol table error size*/
#define DEFAULT_OFFSET 0 /*Starting symbol table db offset*/\


/*Union for data variable types, hold space for largest data size in same memory location*/
typedef union InitialValue {
	int int_val; /* integer variable initial value */
	float fpl_val; /* floating-point variable initial value */
	int str_offset; /* string variable initial value (offset) */
}InitialValue;

/*Stryct for symbol table database records*/
typedef struct SymbolTableVidRecord {
	unsigned short status_field; /* variable record status field*/
	char * plex; /* pointer to lexeme (VID name) in CA */
	int o_line; /* line of first occurrence */
	InitialValue i_value; /* variable initial value */
	size_t reserved; /*reserved for future use*/
}STVR;

/*Struct to create the symbol table DB*/
typedef struct SymbolTableDescriptor {
	STVR *pstvr; /* pointer to array of STVR */
	int st_size; /* size in number of STVR elements */
	int st_offset; /*offset in number of STVR elements */
	Buffer *plsBD; /* pointer to the lexeme storage buffer descriptor */
} STD;

/*Function Prototypes */
STD st_create(int);
int st_install(STD, char*, char, int);
int st_lookup(STD, char*);
int st_update_type(STD, int, char);
int st_update_value(STD, int, InitialValue);
char st_get_type(STD, int);
void st_destroy(STD);
int st_print(STD);
static void st_setsize(void);
static void st_incoffset(void);
int st_store(STD);
int st_sort(STD, char);
#endif

