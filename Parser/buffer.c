/* File name: buffer.c
*  Compiler:  MS Visual Studio 2012
*  Author:	  Brodie Taillefer, 040 757 711
*  Assignment:Buffer Assignment #1
*  Date:      January 29, 2015
*  Professor: Sv. Ranev
*  Purpose:   buffer.c is where we declare all of our functions needed in our program. This includes functions to create the buffer, add chars to the buffer, reset, pack, print the buffer
*			  etc. The functions and their logic are predetermined from the assignment specifications.
*  Function List: Functions include, b_create(),b_addc(),b_reset(),b_isfull(),b_size(),b_capacity(),b_setmark(),b_mark(),b_mode(),b_inc_factor(),b_load(),b_isempty(),b_eob(),b_getc(),b_print()
b_pack(),b_rflag(),b_retract(),b_retract_to_mark(),b_getc_offset(),b_destory()
*/

#include "buffer.h"
/*
* Purpose: Create the buffer given the initial capacity, inc factor and mode. The buffer is created with the standards given in the document and how the buffer will be created with each mode.
*		 Returns NULL, if any parameters are off or if an error occurs.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: calloc(),malloc(),free()
* Parameters: [init_capacity, short value, range  1 - MAX_BUFFER(32767)], [inc_factor, char value, range 0-255], [o_mode, char value, allowed inputs 'a' 'm' 'f']
* Return Value: Returns buffer if succesfull, if not returns NULL
* Algorithm: Check to see if the input parameters, init_capacity,inc_factor and o_mode are valid inputs, if they aren't return NULL. If the inputs are valid, a buffer is created in memory with calloc, to
*			 initialize the structs members to 0.If the buffer was succesfully created, we set the buffer's operating mode and its relevant fields (mode,inc_factor,capacity). The function than returns a pointer
*			 to the buffer that was created.
*/
Buffer * b_create(short init_capacity, char inc_factor, char o_mode) {
	if (init_capacity < MIN_BUFFER || init_capacity > MAX_BUFFER || inc_factor < 0 || (o_mode != MULTIPLICATIVE_CHAR && o_mode != FIXED_CHAR && o_mode != ADDITIVE_CHAR)) { /*If the capacity/inc_factor is less than 0 or greater than MAX_BUFFER(SHRT_MAX) */
		return NULL;
	}
	else {
		Buffer *buffer = (Buffer *)calloc(1, sizeof(Buffer)); /*Calloc a location in memory for a buffer, auto intialize all structure member's to 0 */
		if (buffer == NULL) { //If buffer could not be created
			return NULL;
		}
		buffer->ca_head = (char *)malloc(init_capacity); /*Malloc location in memory for the buffer's ca_head with the intial capacity */
		if (buffer->ca_head == NULL) {
			return NULL;
		}
		if (o_mode == FIXED_CHAR || inc_factor == MIN_INC_FACTOR) { /* Set the operating mode to fixed */
			buffer->mode = FIXED_MODE;
			buffer->inc_factor = MIN_INC_FACTOR;
		}
		else if (o_mode == ADDITIVE_CHAR) { /* Set the operating mode to additive */
			buffer->mode = ADDITIVE_MODE;
			buffer->inc_factor = inc_factor;
		}
		else if (o_mode == MULTIPLICATIVE_CHAR && inc_factor > MIN_INC_FACTOR && inc_factor <= 100) { /* Set the operating mode to multiplicative */
			buffer->mode = MULTIPLICATIVE_MODE;
			buffer->inc_factor = inc_factor;
		}
		else { /*If it wasn't any of those modes we return Null */
			return NULL;
		}
		buffer->capacity = init_capacity;
		return buffer; /* Retur the pointer to the buffer we just created */
	}
}

/*
* Purpose: The purpose of b_addc() is to add a new char into the buffer, resizing the buffer whenever needed and possible
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: malloc(),free()
* Parameters: [init_capacity, short value, range  1 - MAX_BUFFER(32767)], [char symbol, char value, range 0-255],
* Return Value: Returns  pointer to buffer if succesfull, if not returns NULL
* Algorithm: Checks to see if input parameters are valid, and resets the r_flag to 0. If the addc_offset is equal to the capacity, we have a full buffer and must
*			 attempt to resize the buffer. By looking at the buffer's operating mode we attemp increment the buffer's capacity by the guidelines set for each mode.
*            After resize we add the char to the newly sized buffer
*/
pBuffer b_addc(pBuffer const pBD, char symbol) {
	int newMax = 0; /* The new maximum buffer capacity that we will calculate */
	int newIncrement = 0; /* The new increment value that we will calculate */
	char * tempBuffer; /* Create a new char pointer to create a new buffer if we must increase the capacity */
	if (pBD == NULL) {
		return NULL;
	}
	pBD->r_flag = 0; /*If we haven't reached the end of the buffer, reset the r_flag to 0 */
	if (pBD->addc_offset == pBD->capacity) { /* if we reached reached the point where we are at the end of the buffer we must attemp to increase the buffer's capacity */
		if (pBD->capacity == MAX_BUFFER) {
			return NULL;
		}
		if (pBD->mode == FIXED_MODE) { /* If the mode is fixed we cannot resize the buffer */
			return NULL;
		}
		else if (pBD->mode == ADDITIVE_MODE) { /* Addiitive mode */
			newMax = (pBD->inc_factor + pBD->capacity); /* The new buffer's maximum size will be the buffer's inc_factor + the current max size */
			if (newMax > MAX_BUFFER) {
				return NULL;
			}
		}
		else if (pBD->mode == MULTIPLICATIVE_MODE) { /* Multiplicative mode */
			newIncrement = (((MAX_BUFFER - pBD->capacity) * pBD->inc_factor) / 100) * sizeof(char); /*Calculate the new increment value of the buffer */
			if (newIncrement == 0) { /* If the buffer is 0 it has  went over buffer max, reset back to max buffer */
				newMax = MAX_BUFFER;
			}
			else {
				newMax = pBD->capacity + newIncrement; /* New maximum buffer size will be our newIncrement + the current capacity */
			}
		}
		tempBuffer = (char *)realloc(pBD->ca_head, newMax); /* Attempt to realloac a new space in memory for the increased buffer size */
		if (!tempBuffer) {
			return NULL;
		}
		if (pBD->ca_head != tempBuffer) { /* If the memory had to change locations to allocate new space, we must set the r_flag */
			pBD->r_flag = SET_R_FLAG;
		}
		pBD->ca_head = tempBuffer;
		pBD->capacity = (short)newMax;
	}
	pBD->ca_head[pBD->addc_offset++] = symbol; /*Place the symbol at the addc_offset, incrementing for next time */
	return pBD;
}
/*
* Purpose: The purpose of b_reset() is to make the buffer beleive that the next char added should be at the start of the buffer, this mean's resetting
*		   the adc_offset and getc_offset to the start of the buffer.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: calloc(),malloc()
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Returns Success (1) if succesfull, otherwise returns FAIL(0)
* Algorithm: Resets the addc_offset and getc_offset to 0
*/
int b_reset(Buffer * const pBD) {
	if (pBD == NULL) {
		return R_FAIL_1;
	}
	pBD->addc_offset = MIN_BUFFER;
	pBD->getc_offset = MIN_BUFFER;
	pBD->mark_offset = MIN_BUFFER;
	pBD->r_flag = 0;
	pBD->eob = 0;
	return SUCCESS;
}

/*
* Purpose: The purpose of b_isfull() is to check and see if the current buffer is full upto capacity with chars.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Returns R_FAIL_1 (-1) if buffer is invalid, SUCCESS(1) if buffer is full, if not full returns FAIL(0)
* Algorithm: If the addc_offset is equal to the capacity the buffer is full.
*/
int b_isfull(Buffer * const pBD) {
	if (pBD == NULL) {
		return R_FAIL_1;
	}
	if ((pBD->addc_offset * (short)sizeof(char)) == pBD->capacity) { /*If the addc_offset is equal to capacity so the buffer is full */
		return SUCCESS;
	}
	return FAIL; /* Buffer is not full */
}
/*
* Purpose: The purpose of b_size() is to return the current amount of chars located in the buffer.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Returns R_FAIL_1 (-1) if buffer is invalid, else returns the getc_offset(short)
*/
short b_size(Buffer * const pBD) {
	if (pBD == NULL) {
		return R_FAIL_1;
	}
	return pBD->addc_offset;
}

/*
* Purpose: The purpose of b_capacity() is to return the current capacity of the buffer
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Short value, R_FAIL_1(-1) if the buffer is not valid, else returns the capacity of the buffer
*/
short b_capacity(Buffer * const pBD) {
	if (pBD == NULL) {
		return R_FAIL_1;
	}
	return pBD->capacity;
}

/*
* Purpose: The purpose of b_setmark() is to mark a place in the buffer for later use.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid], [short mark, mark must be within range of the buffer and the add location]
* Return Value: Return pointer to the location in memory of the buffer at the location mark, returns NULL if the buffer is invalid or if the mark is out of the range.
* Algorithm: Check to see if the mark and buffer are valid parameters, the mark must be greater than the MIN_BUFFER (0) and lower than the next location of the char that will be added
*/
char * b_setmark(Buffer * const pBD, short mark) {
	if (pBD == NULL) {
		return NULL;
	}
	pBD->mark_offset = mark;
	return &pBD->ca_head[pBD->mark_offset]; /* Return a pointer to the location of the mark_offset in the buffer */
}

/*
* Purpose: The purpose of b_mark() is to return the mark_offset from the start of the buffer.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Return R_FAIL_1(-1) if the buffer is invalid or if the markoffset is greater than or less than the buffer boundaries
*/
short b_mark(Buffer * const pBD) {
	if (pBD == NULL) { /* Check to see if the offset is within range and the buffer is valid */
		return (short)R_FAIL_1;
	}
	return pBD->mark_offset;
}

/*
* Purpose: The purpose of b_mode() is to return the buffer's operating mode.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Return R_FAIL_1(-1) if the buffer is invalid or the operating mode is invalid.
*/
int b_mode(Buffer * const pBD) {
	if (!pBD && (pBD->mode != FIXED_MODE || pBD->mode != MULTIPLICATIVE_MODE || pBD->mode != ADDITIVE_MODE)) { /*Check to see if the mode is valid and the buffer is valid */
		return R_FAIL_1;
	}
	return (int)pBD->mode;
}

/*
* Purpose: The purpose of b_inc_factor() is to return the buffer's inc factor.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Return 256(CHAR_MAX +1) if the inc_factor is not within valid range, or if the buffer is invalid. Return size_t(unsigned int) inc_factor if no errors.
*/
size_t b_inc_factor(Buffer * const pBD) {
	if (pBD == NULL || !pBD || pBD->inc_factor <= CHAR_MIN || pBD->inc_factor > CHAR_MAX) { /* Check if the inc_factor is valid between ( 0-255) and the buffer is valid */
		return (size_t)INC_ERROR;
	}
	return (size_t)pBD->inc_factor;
}

/*
* Purpose: The purpose of b_load() is to read in the file contents, call b_addc() to add the char read from the file into the buffer, and update the get_coffset
*          for the number of chars added
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: b_addc(),feof(),fgetc()
* Parameters:[Constant pointer to a FILE, file must be valid], [Constant pointer to a buffer, buffer must be valid]
* Return Value: Return R_FAIL_1(-1) if the buffer or the FILE is invalid. Returns LOAD_FAIL if the char could not be added. If all is good the number of chars added to the buffer is returned.
* Algorithm: Check to see if the FILE and buffer are valid. Read the in the first char with fgetc() and than test if the end of the file has been reached with Feof()in a while loop. If when trying to
*			 add the char to the you get a return of NULL, this means an error has occured entering a char into the buffer. If all goes well, we increment the char counter, get another char from the file
*			 and continue looping. At the end we must intialize getc_offset by the number of chars that we added to the buffer.
*/
int b_load(FILE * const fi, Buffer * const pBD) {
	char inputChar; /* The char that is read from the file to be inputted into the buffer */
	if (!fi || !pBD){ /*Check if the file and buffer are valid */
		return R_FAIL_1;
	}
	inputChar = (char)fgetc(fi); /* Must get a char before calling feof to see if it's end of file */
	while (!feof(fi)) { /* While we have not reached the end of the file , continue reading the file and adding the char to the buffer */
		if (b_addc(pBD, inputChar) == NULL) { /*If when adding the char to the buffer we get a null return, mean's we could not add it, update the getc_offset and return out */
			return LOAD_FAIL;
		}
		inputChar = (char)fgetc(fi);/*Get next char from file for next while iteration */
	}
	return (int)pBD->getc_offset;
}

/*
* Purpose: The purpose of b_isempty() is to see if the buffer if has any contents inside.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters:[Constant pointer to a buffer, buffer must be valid]
* Return Value: Return R_FAIL_1(-1) if the buffer  is invalid. Returns FAIL (0) if the addc_offset is less than the MIN_BUFFER(0). Returns success(1) if succesfull.
*/
int b_isempty(Buffer * const pBD) {
	if (!pBD) { /* If the buffer is not valid, or the add char offset is lower than MIN_BUFFER (0) */
		return R_FAIL_1;
	}
	if (pBD->addc_offset != MIN_BUFFER) { /* Buffer is not empty */
		return FAIL;
	}
	return SUCCESS;
}

/*
* Purpose: The purpose of b_eob() is to return the eob(end of buffer) flag from the buffer structure.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Return R_FAIL_1(-1) if the buffer is invalid. Else returns eob.
*/
int b_eob(Buffer * const pBD) {
	if (!pBD || pBD == NULL) {
		return R_FAIL_1;
	}
	return pBD->eob;
}

/*
* Purpose: The purpose of b_getc() is to retrieve the char at the adc_offset in the buffer
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Returns R_FAIL_@(-2) if the buffer is invalid or if the addc_offset or getc_offset is Less than MIN_BUFFER(0). If the end of the buffer is reached R_FAIL_1(-1) is returned. Else we return the char
that was retrieved from the buffer at location getc_offset.
* Algorithm: If the addc_offset is equal to getc_offset we have reached the end of the buffer, so we set the eob flag to 1, if we havent reached the end of buffer, we retrieve the next char in the pointer to
the buffer. This means we aren't at the end, so we reset the end of buffer flag.
*/
char b_getc(Buffer * const pBD) {
	char nextChar; //Store the char pointed to at index getc_offset for return
	if (!pBD || pBD->addc_offset <= MIN_BUFFER || pBD->getc_offset < MIN_BUFFER) {
		return R_FAIL_2;
	}
	if (pBD->addc_offset == pBD->getc_offset) { /* If addc_Offset = getc_offset we have reached the end of buffer */
		pBD->eob = SET_EOB;  /* Set the end of buffer flag */
		return R_FAIL_1;
	}
	pBD->eob = FAIL;
	nextChar = pBD->ca_head[pBD->getc_offset++]; /* Initialize the char that is at location getc_offset of the buffer, increment for next call */
	return nextChar;
}

/*
* Purpose: The purpose of b_print() is to print out the buffer char by char by using the functions we created, b_getc, b_isempty,b_eob.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: b_isempty(),b_getc(),b_eob()
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Returns R_FAIL_1(-1)  if the buffer is invalid or the buffer is empty. Else we return the number of chars that we printed out from the buffer.
* Algorithm: If the buffer is empty, we have nothing to print. Set the getc_offset to the start of the buffer. Else use b_getc() to get the next char and check to see if we have reached the end of the buffe
*			 if not, we print that char, increment our char counter and get the next char, repeating until we reach the end.
*/
int b_print(Buffer * const pBD) {
	int numChars = 0; /* keep track of the number of chars we print */
	char nextChar; /* The char to print out */
	if (!pBD) {
		return R_FAIL_1;
	}
	if (b_isempty(pBD)) { /* If the buffer is empty, we cannot print anything */
		printf("The buffer is empty.\n");
		pBD->getc_offset = MIN_BUFFER; /* Reset the getc_offset */
		return R_FAIL_1;
	}
	pBD->getc_offset = MIN_BUFFER; /* Reset the getc_offset to begin printing at the start of the buffer */
	nextChar = b_getc(pBD); /* Get first char from the buffer */
	while (!b_eob(pBD)) { /* While we have not reached the end of the buffer continue printing */
		printf("%c", nextChar);
		numChars++;
		nextChar = b_getc(pBD);
	}
	printf("\n");
	pBD->getc_offset = MIN_BUFFER;
	return numChars;
}

/*
* Purpose: The purpose of b_pack() is to condense the buffer into the smallest capacity that will fit the buffer, and reconfiguring all the buffer member's
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: realloc()
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Returns NULL if the the buffer is invalid or the capacity is not valid. Else we return a pointer to the new packed buffer.
* Algorithm: Calculate the new capacity at 1 more than our next add location, reallocing a new pointer for the new sized buffer. Check to see if the buffer has moved locations in memory, if it has set the
relocation flag. Update the buffer members to their new locations.
*/
Buffer *b_pack(Buffer * const pBD) {
	char *tempBuffer; /*Temporary pointer in memory to hold the location of the new packed buffer*/
	short newCapacity; /*Used to calculate the new size that the buffer has to be packed to */
	if (!pBD || !pBD->ca_head) {
		return NULL;
	}
	newCapacity = (pBD->addc_offset + 1); /* The new capacity of the buffer will be the next add location +1 */
	tempBuffer = (char *)realloc(pBD->ca_head, newCapacity); /* Realloc a new space memory for the decreased buffer size */
	if (!tempBuffer) {
		return NULL;
	}
	if (tempBuffer != pBD->ca_head) { /*If we had to get a new location in memory we must set the r_flag */
		pBD->r_flag = SET_R_FLAG;
		pBD->ca_head = tempBuffer;
	}
	pBD->getc_offset = newCapacity;
	pBD->capacity = newCapacity;
	return pBD;
}

/*
* Purpose: The purpose of b_rflag() is to see if the buffer's relocation flag is set
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Returns R_FAIL_1 (-1) if the buffer is invalid.
*/
char b_rflag(Buffer * const pBD) {
	if (!pBD) {
		return R_FAIL_1;
	}
	return pBD->r_flag;
}

/*
* Purpose: The purpose of b_retract() is to retract the getc_offset back one char in the buffer
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Returns R_FAIL_1 (-1) if the buffer is invalid or the char offset -1 is less than 0
*/
short b_retract(Buffer * const pBD) {
	if (pBD == NULL || pBD->getc_offset == 0) {
		return R_FAIL_1;
	}
	pBD->getc_offset--;/* Retract the getc_offset back 1 char in the buffer */
	return SUCCESS;
}

/*
* Purpose: The purpose of b_retract_to_mark() is to retrack the get char location the buffer to the mark location that is set by mark_offset
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Returns R_FAIL_1 (-1) if the buffer is invalid or the mark_offset is less than MIN_BUFFER(0)
*/
short b_retract_to_mark(Buffer * const pBD) {
	if (!pBD) {
		return R_FAIL_1;
	}
	pBD->getc_offset = pBD->mark_offset; /* Set the getc_offset equal to the mark offset */
	return pBD->getc_offset;
}

/*
* Purpose: The purpose of b_getc_offet is to retrieve the current get char offset location in the buffer
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: No functions called
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: Returns R_FAIL_1 (-1) if the buffer is invalid or the char offset is less than 0
*/
short b_getc_offset(Buffer * const pBD) {
	if (!pBD) {
		return R_FAIL_1;
	}
	return pBD->getc_offset;
}

/*
* Purpose: The purpose of b_destroy() is to free all dynamic memory associated from the buffer in memory, preventing any memory leaks or dangling pointers
* Author:  Brodie Taillefer
* History/Revisions: V1.0 January 23, 2015
* Called functions: free()
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: no return value (void)
*/
void b_destroy(Buffer * const pBD) {
	if (pBD != NULL && pBD->ca_head != NULL) {
		free(pBD->ca_head); /* Free the dynamic memory for the buffer head */
		free(pBD);
	}
}

/*
* Purpose: The purpose of b_getaddc_offset() is to return the buffer's addc_offset
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 1, 2015
* Called functions: none
* Parameters: [Constant pointer to a buffer, buffer must be valid]
* Return Value: return short, addc_offset
*/
short b_getaddc_offset(Buffer * const pBD) {
	if (!pBD) {
		return FAIL;
	}
	return pBD->addc_offset;
}
