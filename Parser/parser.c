/* File name: parser.c
*  Compiler:  MS Visual Studio 2012
*  Author:	  Brodie Taillefer, 040 757 711
*  Assignment:Parser #4
*  Date:      April 10, 2015
*  Professor: Sv. Ranev
*  Purpose:  Implements a Recursive Descent Predictive Parser(RDPP) converting the grammars production's into functions, and implementing the parsers support functions
*  Function List: parser(),match(),syn_printe(),gen_incode(),syn_eh(),assignment_statement(),assignment_expression(),selection_statement(),iteration_statement(),input_statement(),variable_list(),variable_identifier(),variable_list_p(),output_statement(),opt_variable_list(),opt_statements(),
*                 arithmetic_expression(),unary_arithmetic_expression(),primary_arithmetic_expression(),additive_arithmetic_expression(),additive_arithmetic_expression_p(),multiplicative_arithmetic_expression(),multiplicative_arithmetic_expression_p(),string_expression(),string_expression_p(),
*                 primary_string_expression()conditional_expression(),logical_and_expression(),logical_or_expression(),logical_and_expression_p(),logical_or_expression_p(),relational_expression(),primary_a_relational_expression(),primary_a_relational_expression_p(),primary_s_relational_expression(),
				  primary_s_relational_expression_p()
*/
#include "parser.h"

/*
* Purpose: Start the parsing of the program, intializes the Buffer, grab the first token, and begin matching the first production
* Author:  Brodie Taillefer
* History/Revisions: V1.0 April 10, 2015
* Called functions: mlwpar_next_token(),program(),match(),gen_incode()
* Parameters: [Buffer *, valid pointer]
* Return Value: void return
*/
void parser(Buffer*  in_buf) {
	sc_buf = in_buf;
	lookahead = mlwpar_next_token(sc_buf); /*Get the first token */
	program(); match(SEOF_T, NO_ATTR); /*Match the program production, and SEOF */
	gen_incode("PLATY: Source file parsed"); 
}

/*
* Purpose: Match the current token with the needed token to match the production, uses the token attribute if needed
* Author:  Brodie Taillefer
* History/Revisions: V1.0 April 10, 2015
* Called functions: mlwpar_next_token(),syn_eh(),syn_printe()
* Parameters: [int pr_token_code, none] , [int pr_token_attribute, none]
* Return Value: void
*/
void match(int pr_token_code, int pr_token_attribute) {
	if (pr_token_code == lookahead.code) { /*If the token code matches the current lookahead token */
		if ((lookahead.code == KW_T && lookahead.attribute.get_int != pr_token_attribute) || (lookahead.code == LOG_OP_T && lookahead.attribute.log_op != pr_token_attribute) /*If we have a keyword token we should use the attribute to see if they match */
			|| (lookahead.code == ART_OP_T && lookahead.attribute.arr_op != pr_token_attribute) || (lookahead.code == REL_OP_T && lookahead.attribute.rel_op != pr_token_attribute)) {
			syn_eh(pr_token_code); /*Didn't match, call error function */
			return;
		}
		if (lookahead.code == SEOF_T) { /*If we matched SEOF, return */
			return;
		}
		lookahead = mlwpar_next_token(sc_buf); /*Advance to next token */
		if (lookahead.code == ERR_T) { /*Is the next token an error token ? */
			syn_printe(); /*Print the error */
			lookahead = mlwpar_next_token(sc_buf); /*Advance token */
			++synerrno; /*Increment syntax error counter */
		}
		return; /*Matched, return */
	}
	syn_eh(pr_token_code); /*Didn't match initially, error */
}

/*
* Purpose: Print the current syntax/grammar error
* Author:  Svillen Ranev
* History/Revisions: V1.0 April 10, 2015
* Called functions: printf()
* Parameters: none
* Return Value: Return void
*/
void syn_printe(){
	Token t = lookahead;
	printf("PLATY: Syntax error:  Line:%3d\n", line);
	printf("*****  Token code:%3d Attribute: ", t.code);
	switch (t.code){
	case  ERR_T: /* ERR_T     0   Error token */
		printf("%s\n", t.attribute.err_lex);
		break;
	case  SEOF_T: /*SEOF_T    1   Source end-of-file token */
		printf("NA\n");
		break;
	case  AVID_T: /* AVID_T    2   Arithmetic Variable identifier token */
	case  SVID_T:/* SVID_T    3  String Variable identifier token */
		printf("%s\n", sym_table.pstvr[t.attribute.get_int].plex);
		break;
	case  FPL_T: /* FPL_T     4  Floating point literal token */
		printf("%5.1f\n", t.attribute.flt_value);
		break;
	case INL_T: /* INL_T      5   Integer literal token */
		printf("%d\n", t.attribute.get_int);
		break;
	case STR_T:/* STR_T     6   String literal token */
		printf("%s\n", b_setmark(str_LTBL, t.attribute.str_offset));
		break;
	case SCC_OP_T: /* 7   String concatenation operator token */
		printf("NA\n");
		break;
	case  ASS_OP_T:/* ASS_OP_T  8   Assignment operator token */
		printf("NA\n");
		break;
	case  ART_OP_T:/* ART_OP_T  9   Arithmetic operator token */
		printf("%d\n", t.attribute.get_int);
		break;
	case  REL_OP_T: /*REL_OP_T  10   Relational operator token */
		printf("%d\n", t.attribute.get_int);
		break;
	case  LOG_OP_T:/*LOG_OP_T 11  Logical operator token */
		printf("%d\n", t.attribute.get_int);
		break;
	case  LPR_T: /*LPR_T    12  Left parenthesis token */
		printf("NA\n");
		break;
	case  RPR_T: /*RPR_T    13  Right parenthesis token */
		printf("NA\n");
		break;
	case LBR_T: /*    14   Left brace token */
		printf("NA\n");
		break;
	case RBR_T: /*    15  Right brace token */
		printf("NA\n");
		break;
	case KW_T: /*     16   Keyword token */
		printf("%s\n", kw_table[t.attribute.get_int]);
		break;
	case COM_T: /* 17   Comma token */
		printf("NA\n");
		break;
	case EOS_T: /*    18  End of statement *(semi - colon) */
		printf("NA\n");
		break;
	default:
		printf("PLATY: Scanner error: invalid token code: %d\n", t.code);
	}/*end switch*/
}/* end syn_printe()*/

/*
* Purpose: Prints the parameter to the screen
* Author:  Brodie Taillefer
* History/Revisions: V1.0 April 10, 2015
* Called functions: printf()
* Parameters: [char *, valid pointer]
* Return Value: void return
*/
void gen_incode(char *string) {
	if(string) {
		printf("%s\n", string);
	}
}

/*
* Purpose: Panic mode error recovery, tries to find the needed token that the parser is looking for.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 April 10, 2015
* Called functions: syn_printe(),mlwpar_next_token(),exit()
* Parameters: [int sync_token_code, none]
* Return Value: void return
*/
void syn_eh(int sync_token_code) {
	syn_printe(); /*Print the error */
	++synerrno; /*Increment error counter */
	if (sync_token_code == SEOF_T) { /*If we are looking for SEOF, return */
		return;
	}
	while (lookahead.code != SEOF_T) { /*While the current token is not SEOF */
		lookahead = mlwpar_next_token(sc_buf); /*Grab the next token */
		if (lookahead.code == sync_token_code) { /*If we have found a match */
			lookahead = mlwpar_next_token(sc_buf); /*Grab the next token and return */
			return;
		}
	}
	exit(synerrno); /*Else we reached SEOF before we found the other token, we should quit */
}

/*  <program> -> PLATYPUS {<opt_statements>};
FIRST SET { PLATYPUS }
*/
void program(void) {
	match(KW_T, PLATYPUS);
	match(LBR_T, NO_ATTR);
	opt_statements();
	match(RBR_T, NO_ATTR);
	gen_incode("PLATY: Program parsed");
}

/*	<statement> - > <assignment_statement> | <selection_statement> | <iteration_statement> | <input_statement> | <output_statement>
FIRST SET { AVID_T,SVID_T,IF,USING,INPUT,OUTPUT}
*/
void statement(void) {
	if (lookahead.code == AVID_T || lookahead.code == SVID_T) { /*If the next token is AVID or SVID */
		assignment_statement();
		return;
	}
	if (lookahead.code == KW_T && lookahead.attribute.kwt_idx == IF) { /*If the next token is keyword IF */
		selection_statement();
		return;
	}
	if (lookahead.code == KW_T && lookahead.attribute.kwt_idx == USING) { /*If the next token is keyword USING */
		iteration_statement();
		return;
	}
	if (lookahead.code == KW_T && lookahead.attribute.kwt_idx == INPUT) { /*If the next token is keyword INPUT */
		input_statement();
		return;
	}
	if (lookahead.code == KW_T && lookahead.attribute.kwt_idx == OUTPUT) { /*if the next token is keyword OUTPUT */
		output_statement();
		return;
	}
	syn_printe(); /*Else we have an error */
}

/*	<statements> -> <statement><statements_p>
FIRST SET {AVID, SVID, IF, USING, INPUT, OUTPUT}
*/
void statements(void) {
	statement();
	statements_p();
}

/*	<statements_p> -> <statement><statements_p> | E
FIRST SET { AVID, SVID, IF, USING, INPUT, OUTPUT}
*/
void statements_p(void) {
	if (lookahead.code == KW_T && (lookahead.attribute.kwt_idx == PLATYPUS || lookahead.attribute.kwt_idx == THEN || lookahead.attribute.kwt_idx == ELSE || lookahead.attribute.kwt_idx == REPEAT)) { /*If the token is keyword and not the following, IF,USING,INPUT or OUTPUT */
		return;
	}
	else if (lookahead.code == AVID_T || lookahead.code == SVID_T || lookahead.code == KW_T) { /*If the token is AVID, SVID or any other keyword */
		statement();
		statements_p();
	}
}

/*	<assignment_statement> -> <assignment_expression>;
FIRST SET {AVID_T,SVID_T}
*/
void assignment_statement(void) {
	assignment_expression();
	match(EOS_T, NO_ATTR);
	gen_incode("PLATY: Assignment statement parsed");
}

/* <assignment_expression> -> AVID = <arithmetic_expression> | SVID = <string_expression>
FIRSTSET{ SVID, AVID}
*/
void assignment_expression(void) {
	if (lookahead.code == AVID_T) { /*If the token is AVID */
		match(AVID_T, NO_ATTR);
		match(ASS_OP_T, NO_ATTR);
		arithmetic_expression();
		gen_incode("PLATY: Assignment expression (arithmetic) parsed");
		return;
	}
	if (lookahead.code == SVID_T) { /*If the token is SVID */
		match(SVID_T, NO_ATTR);
		match(ASS_OP_T, NO_ATTR);
		string_expression();
		gen_incode("PLATY: Assignment expression (string) parsed");
		return;
	}
	syn_printe(); /*Else print the error */
}

/* <selection_statement> -> IF(<conditional_expression>) THEN <opt_statements> ELSE { <opt_statements> };
FIRSTSET { IF }
*/
void selection_statement(void) {
	match(KW_T, IF);
	match(LPR_T, NO_ATTR);
	conditional_expression();
	match(RPR_T, NO_ATTR);
	match(KW_T, THEN);
	opt_statements();
	match(KW_T, ELSE);
	match(LBR_T, NO_ATTR);
	opt_statements();
	match(RBR_T, NO_ATTR);
	match(EOS_T, NO_ATTR);
	gen_incode("PLATY: IF statement parsed");
}

/* <iteration_statement> -> USING (<assignment_expression>,<conditional_expression>,<assignment_expression>) REPEAT {<opt_statements>};
FIRSTSET { USING }
*/
void iteration_statement(void) {
	match(KW_T, USING);
	match(LPR_T, NO_ATTR);
	assignment_expression();
	match(COM_T, NO_ATTR);
	conditional_expression();
	match(COM_T, NO_ATTR);
	assignment_expression();
	match(RPR_T, NO_ATTR);
	match(KW_T, REPEAT);
	match(LBR_T, NO_ATTR);
	opt_statements();
	match(RBR_T, NO_ATTR);
	match(EOS_T, NO_ATTR);
	gen_incode("PLATY: USING statement parsed");
}

/* <input_statement> -> INPUT {<variable_list};
FIRSTSET {INPUT}
*/
void input_statement(void) {
	match(KW_T, INPUT);
	match(LPR_T, NO_ATTR);
	variable_list();
	match(RPR_T, NO_ATTR);
	match(EOS_T, NO_ATTR);
	gen_incode("PLATY: INPUT statement parsed");
}
/*	<variable_list> -> <variable_identifier><variable_list_p>
FIRST SET {AVID_T, SVID_T}
*/
void variable_list(void) {
	variable_identifier();
	variable_list_p();
	gen_incode("PLATY: Variable list parsed");
}

/* <variable_identifier> -> AVID_T | SVID_T
FIRST SET { }
*/
void variable_identifier(void) {
	if (lookahead.code == AVID_T || lookahead.code == SVID_T) { /*If the token is AVID or SVID */
		match(lookahead.code, NO_ATTR);
		return;
	}
	syn_printe();
}

/*	<variable_list_p> -> , <variable_indentifier><variable_list_p> | E
FIRST SET {, , E}
*/
void variable_list_p(void) {
	if (lookahead.code == COM_T) { /*If the token is , */
		match(COM_T, NO_ATTR);
		variable_identifier();
		variable_list_p();
	}
	else {
		return;
	}
}

/* <output_statement> -> OUTPUT (<opt_variable_list>);
FIRSTSET { OUTPUT }
*/
void output_statement(void) {
	match(KW_T, OUTPUT);
	match(LPR_T, NO_ATTR);
	opt_variable_list();
	match(RPR_T, NO_ATTR);
	match(EOS_T, NO_ATTR);
	gen_incode("PLATY: OUTPUT statement parsed");
}

/* <opt_variable_list> -> <variable_list> | STR_T | E
FIRSTSET { AVID_T,SVID_T,STR_T,E}
*/
void opt_variable_list(void) {
	if (lookahead.code == AVID_T || lookahead.code == SVID_T) { /*if the token is AVID or SVID */
		variable_list();
	}
	else if (lookahead.code == STR_T) { /*if the token is STR literal */
		match(STR_T, NO_ATTR);
		gen_incode("PLATY: Output list (string literal) parsed");
	}
	else { /*Else we have an empty list */
		gen_incode("PLATY: Output list (empty) parsed");
	}
}

/*	<opt_statements> -> <statements> | E
FIRST SET { AVID, SVID, IF, USING, INPUT, OUTPUT }
*/
void opt_statements(void) {
	if (lookahead.code == AVID_T || lookahead.code == SVID_T || lookahead.code == KW_T && (lookahead.attribute.get_int == IF || lookahead.attribute.get_int == USING
		|| lookahead.attribute.get_int == INPUT || lookahead.attribute.get_int == OUTPUT)) { /*If the token is AVID,SVID or keyword and IF or USING or INPUT or OUTPUT */
		statements();
	}
	else {
		gen_incode("PLATY: Opt_statements parsed");
	}
}

/*	<arithmetic_expression> -> <unary_arithmetic_expression> | <additive_arithmetic_expression>
FIRSTSET {-,+,AVID_T,FPL_T,INL_T}
*/
void arithmetic_expression(void) {
	if (lookahead.code == ART_OP_T && (lookahead.attribute.arr_op == MINUS || lookahead.attribute.arr_op == PLUS)){ /*If keyword is arithmetic operator and + or - */
		unary_arithmetic_expression();
	}
	else if (lookahead.code == AVID_T || lookahead.code == FPL_T || lookahead.code == INL_T || lookahead.code == LPR_T) { /*if keyword is AVID,FPL,INL or ( */
		additive_arithmetic_expression();
	}
	else { /*else we have an error */
		syn_printe();
		return;
	}
	gen_incode("PLATY: Arithmetic expression parsed");
}

/*  <unary_arithmetic_expression> -> - <primary_arithmetic_expression> | + <primary_arithmetic_expresison>
FIRSTSET {-,+}
*/
void unary_arithmetic_expression(void) {
	if (lookahead.code == ART_OP_T && (lookahead.attribute.arr_op == MINUS || lookahead.attribute.arr_op == PLUS)) { /*If token is arithmetic operator and + or - */
		match(lookahead.code, lookahead.attribute.arr_op);
		primary_arithmetic_expression();
		gen_incode("PLATY: Unary arithmetic expression parsed");
		return;
	}
	syn_printe();
}

/* <primary_arithmetic_expression> -> AVID_T | FPL_T | INL_T | (<arithmetic_expression>)
FIRSTSET {AVID_T,FPL_T, INL_T,( }
*/
void primary_arithmetic_expression(void) {
	if (lookahead.code == AVID_T || lookahead.code == FPL_T || lookahead.code == INL_T) { /*If token is AVID,SVID or INL */
		match(lookahead.code, lookahead.attribute.arr_op);
	}
	else if (lookahead.code == LPR_T) { /*If token is ( */
		match(lookahead.code, lookahead.attribute.arr_op);
		arithmetic_expression();
		match(RPR_T, NO_ATTR);
	}
	else { /*Else error */
		syn_printe();
		return;
	}
	gen_incode("PLATY: Primary arithmetic expression parsed");

}

/* <additive_arithmetic_expression> -> <multiplicative_arithmetic_expression><additive_arithmetic_expression_p>
FIRSTSET { AVID_T,FPL_T,INL_T, ( }
*/
void additive_arithmetic_expression(void) {
	multiplicative_arithmetic_expression();
	additive_arithmetic_expression_p();
}


/* <additive_arithmetic_expression_p> -> +<multiplicative_arithmetic_expression><additive_arithmetic_expression_prime> |
										 -<multiplicative_arithmetic_expression><additive_arithmetic_expression_prime> |
										 E
FIRSTSET { +,-,E}
*/
void additive_arithmetic_expression_p(void) {
	if (lookahead.code == ART_OP_T && lookahead.attribute.arr_op != MULT && lookahead.attribute.arr_op != DIV) { /*If token is arithmetic operator and + or - */
		match(lookahead.code, lookahead.attribute.arr_op);
		multiplicative_arithmetic_expression();
		additive_arithmetic_expression_p();
		gen_incode("PLATY: Additive arithmetic expression parsed");
	}
}

/* <multiplicative_arithmetic_expression> -> <primary_arithmetic_expression><mulitplicative_arithmetic_expression_p>
FIRSTSET { AVID_T,FPL_T,INL_T, ( }
*/
void multiplicative_arithmetic_expression(void) {
	primary_arithmetic_expression();
	multiplicative_arithmetic_expression_p();
}

/* <multiplicative_arithmetic_expression_p> -> *<primary_arithmetic_expression><multiplicative_arithmetic_expression_prime>|
											   /<primary_arithmetic_expression><multiplicative_arithmetic_expression_prime>|
											   E
FIRSTSET {*, / , E}
*/
void multiplicative_arithmetic_expression_p(void) {
	if (lookahead.code == ART_OP_T && (lookahead.attribute.arr_op == DIV || lookahead.attribute.arr_op == MULT)) { /*If token is arithmetic operator and / or * */
		match(lookahead.code, lookahead.attribute.arr_op);
		primary_arithmetic_expression();
		multiplicative_arithmetic_expression_p();
		gen_incode("PLATY: Multiplicative arithmetic expression parsed");
	}
}

/* <string_expression> -> <primary_string_expression><string_expression_p>
FIRSTSET { SVID_T,STR_T }
*/
void string_expression(void) {
	primary_string_expression();
	string_expression_p();
	gen_incode("PLATY: String expression parsed");
}

/* >string_expression_p> -> << <primary_string_expression><string_expression_p> | �E
FIRSTSET { << , E}
*/
void string_expression_p(void) {
	if (lookahead.code == SCC_OP_T) { /*If token is << */
		match(SCC_OP_T, NO_ATTR);
		primary_string_expression();
		string_expression_p();
	}
}

/* <primary_string_expression> -> SVID_T | STR_T
FIRSTSET { SVID_T,STR_T }
*/
void primary_string_expression(void) {
	if (lookahead.code == SVID_T || lookahead.code == STR_T) { /*If token is SVID or STR literal */
		match(lookahead.code, NO_ATTR);
	}
	else { /* Else error */
		syn_printe();
	}
	gen_incode("PLATY: Primary string expression parsed");
}

/* <conditional_expression> -> <logical_or_expression>
FIRSTSET { AVID_T, FPL_T, INL_T, SVID_T, STR_T }
*/
void conditional_expression(void) {
	logical_or_expression();
	gen_incode("PLATY: Conditional expression parsed");
}

/* <logical_or_expression> -> <logical_and_expression><logical_or_expression_p>
FIRSTSET {AVID_T, FPL_T, INL_T, SVID_T, STR_T }
*/
void logical_or_expression(void) {
	logical_and_expression();
	logical_or_expression_p();
}

/* <logical_and_expression> -> <relational_expression><logical_and_expression_p>
FIRSTSET { AVID_T, FPL_T, INL_T, SVID_T, STR_T }
*/
void logical_and_expression(void) {
	relational_expression();
	logical_and_expression_p();
}

/* <logical_and_expression_p> -> .AND. <relational_expression><logical_AND_expression_prime> | E
FIRSTSET {.AND. , E }
*/
void logical_and_expression_p(void) {
	if (lookahead.code == LOG_OP_T && lookahead.attribute.log_op == AND) { /*If token is log operator and .AND. */
		match(LOG_OP_T, AND);
		relational_expression();
		logical_and_expression_p();
		gen_incode("PLATY: Logical AND expression parsed");
	}
}

/* <logical_or_expression_p> -> .OR. <logical_AND_expression><logical_OR_expression_prime> | E
FIRSTSET {.OR. , E}
*/
void logical_or_expression_p(void) {
	if (lookahead.code == LOG_OP_T && lookahead.attribute.log_op == OR) { /*If token is log operator and .OR. */
		match(LOG_OP_T, OR);
		logical_and_expression();
		logical_or_expression_p();
		gen_incode("PLATY: Logical OR expression parsed");
	}
}

/* <relational_expression> -> <primary_a_relational_expression><primary_a_relational_expression_p>|
							  <primary_s_relational_expression><primary_s_relational_expression_p>
FIRSTSET {AVID_T, FPL_T, INL_T, SVID_T, STR_T }
*/
void relational_expression(void) {
	if (lookahead.code == AVID_T || lookahead.code == FPL_T || lookahead.code == INL_T) { /* If token is AVID, FPL or INL */
		primary_a_relational_expression();
		primary_a_relational_expression_p();
	}
	else if (lookahead.code == STR_T || lookahead.code == SVID_T) { /*if Token is STR literal or SVID */
		primary_s_relational_expression();
		primary_s_relational_expression_p();
	}
	else {
		syn_printe();
	}
	gen_incode("PLATY: Relational expression parsed");
}

/* <primary_a_relational_expression> -> AVID_T | FPL_T | INL_T
FIRSTSET { AVID_T, FPL_T, INL_T}
*/
void primary_a_relational_expression(void) {
	if (lookahead.code == FPL_T || lookahead.code == AVID_T || lookahead.code == INL_T) { /*If token is FPL,AVID or INL */
		match(lookahead.code, lookahead.attribute.rel_op);
	}
	else { /* Else we have an error */
		syn_printe();
	}
	gen_incode("PLATY: Primary a_relational expression parsed");
}

/* <primary_a_relational_expression_p> -> == <primary_a_relational_expression><primary_a_relational_expression_prime>|
										  <> <primary_a_relational_expression><primary_a_relational_expression_prime>|
										   > <primary_a_relational_expression><primary_a_relational_expression_prime>|
										   < <primary_a_relational_expression><primary_a_relational_expression_prime>
FIRSTSET { ==, <>, >, < } 
*/
void primary_a_relational_expression_p(void) {
	if (lookahead.code == REL_OP_T && (lookahead.attribute.rel_op == EQ || lookahead.attribute.rel_op == NE || lookahead.attribute.rel_op == LT || lookahead.attribute.rel_op == GT)) { /* If token is relational operator and =,<>,<,> */
		match(lookahead.code, lookahead.attribute.arr_op);
		primary_a_relational_expression();
		return;
	}
	syn_printe();
}

/* <primary_s_relational_expression> -> <primary_string_expression>
FIRSTSET { SVID_T, STR_T }
*/
void primary_s_relational_expression(void) {
	if (lookahead.code == SVID_T || lookahead.code == STR_T) { /*If token is SVID, or STR literal */
		primary_string_expression();
	}
	else { /*Else we have an error */
		syn_printe();
	}
	gen_incode("PLATY: Primary s_relational expression parsed");
}

/* <primary_s_relational_expression_p> -> == <primary_s_relational_expression><primary_s_relational_expression_prime>|
										  <> <primary_s_relational_expression><primary_s_relational_expression_prime>|
										   > <primary_s_relational_expression><primary_s_relational_expression_prime>|
										   < <primary_s_relational_expression><primary_s_relational_expression_prime>
FIRSTSET { ==,<>,<,>}
*/
void primary_s_relational_expression_p(void) {
	if (lookahead.code == REL_OP_T && (lookahead.attribute.rel_op == EQ || lookahead.attribute.rel_op == NE || lookahead.attribute.rel_op == LT || lookahead.attribute.rel_op == GT)) { /*If token is relational operator and =,<>,<,> */
		match(lookahead.code, lookahead.attribute.arr_op);
		primary_s_relational_expression();
		return;
	}
	syn_printe();
}
