/* File name: parser.h
*  Compiler:  MS Visual Studio 2012
*  Author:	  Brodie Taillefer, 040 757 711
*  Assignment:Parser #4
*  Date:     April 10, 2015
*  Professor: Sv. Ranev
*  Purpose:  Header file for parser.c, includes extern declarations, global declaration, and function prototypes
*  Function List: No functions , just function prototypes.
*/
#ifndef PARSER_H_
#define PARSER_H_

#include "token.h"
#include "stable.h"
#include "buffer.h"
#include <stdlib.h>

#define NO_ATTR -1
static Token lookahead; /*Create Token for lookahead token */
static Buffer* sc_buf; /*Extern declaration for Buffer * */
int synerrno; /* Counter for # of syntax errors */
extern Token mlwpar_next_token(Buffer*); /*Extern declaration for function mlwpar_next_token */
extern STD sym_table; /*Extern declaration to global symbol table */
extern int line; /*Extern declration for line number */
extern Buffer * str_LTBL; /*Extern declaration to string literal buffer */
extern char *kw_table[8]; /*Extern pointer to the kw_table */

typedef enum { ELSE, IF, INPUT, OUTPUT, PLATYPUS, REPEAT, THEN, USING } keywords; /*Enumeration for our keywords, starting at 0 */


/*function prototypes */
void match(int, int);
void parser(Buffer *);
void syn_eh(int);
void syn_printe();
void program(void);
void gen_incode(char*);
void opt_statements(void);
void input_statements(void);
void variable_list(void);
void variable_list_p(void);
void variable_identifier(void);
void statement(void);
void statements(void);
void statements_p(void);
void assignment_statement(void);
void selection_statement(void);
void iteration_statement(void);
void input_statement(void);
void output_statement(void);
void assignment_expression(void);
void additive_arithmetic_expression(void);
void multiplicative_arithmetic_expression(void);
void additive_arithmetic_expression_p(void);
void multiplicative_arithmetic_expression_p(void);
void string_expression(void);
void logical_or_expression(void);
void conditional_expression(void);
void opt_variable_list(void);
void output_statement(void);
void arithmetic_expression(void);
void unary_arithmetic_expression(void);
void primary_arithmetic_expression(void);
void primary_string_expression(void);
void string_expresison_p(void);
void logical_and_expression(void);
void logical_or_expression_p(void);
void logical_and_expression_p(void);
void relational_expression(void);
void output_list(void);
void string_expression_p(void);
void primary_a_relational_expression(void);
void primary_a_relational_expression_p(void);
void primary_s_relational_expression(void);
void primary_s_relational_expression_p(void);
void opt_variable_list_p(void);
#endif
