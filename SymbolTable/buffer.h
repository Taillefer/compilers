/* File name: buffer.h
*  Compiler:  MS Visual Studio 2012
*  Author:	  Brodie Taillefer, 040 757 711
*  Assignment:Buffer Assignment #1
*  Date:      TBD
*  Professor: Sv. Ranev
*  Purpose:   Header file for buffer.c, includes preprocessor directives, type declartions and prototypes necessary for the buffer implementation.
*  Function List: No functions declared in the file, just function prototypes.
*/

#ifndef BUFFER_H_
#define BUFFER_H_

/*#pragma warning(1:4001) *//*to enforce C89 type comments  - to make //comments an warning */

/*#pragma warning(error:4001)*//* to enforce C89 comments - to make // comments an error */

/* standard header files */
#include <stdio.h>  /* standard input/output */
#include <malloc.h> /* for dynamic memory allocation*/
#include <limits.h> /* implementation-defined data type ranges and limits */

/* constant definitions */
/* You may add your own constant definitions here */
#define R_FAIL_1 -1         /* fail return value */
#define R_FAIL_2 -2         /* fail return value */
#define LOAD_FAIL -2       /* load fail error */
#define SET_R_FLAG 1       /* realloc flag set value */
#define MIN_BUFFER 0
#define MAX_BUFFER SHRT_MAX
#define SUCCESS 1
#define FAIL 0
#define MULTIPLICATIVE_CHAR 'm'
#define FIXED_CHAR 'f'
#define ADDITIVE_CHAR 'a'
#define MULTIPLICATIVE_MODE -1
#define FIXED_MODE 0
#define ADDITIVE_MODE 1
#define MIN_INC_FACTOR 0
#define SET_EOB 1
#define INC_ERROR 256
/* user data type declarations */
typedef struct BufferDescriptor {
	char *ca_head;   /* pointer to the beginning of character array (character buffer) */
	short capacity;    /* current dynamic memory size (in bytes) allocated to character buffer */
	short addc_offset;  /* the offset (in chars) to the add-character location */
	short getc_offset;  /* the offset (in chars) to the get-character location */
	short mark_offset; /* the offset (in chars) to the mark location */
	char  inc_factor; /* character array increment factor */
	char  r_flag;     /* character array reallocation flag */
	char  mode;       /* operational mode indicator*/
	int   eob;       /* end-of-buffer reached flag */
} Buffer, *pBuffer;
/*typedef Buffer *pBuffer;*/

/* function declarations */
/*
Place your function declarations here.
Do not include the function header comments here.
Place them in the buffer.c file
*/
Buffer * b_create(short init_capacity, char inc_factor, char o_mode);
pBuffer b_addc(pBuffer const pBD, char symbol);
int b_reset(Buffer * const pBD);
void b_destroy(Buffer * const pBD);
int b_isfull(Buffer * const pBD);
short b_size(Buffer * const pBD);
short b_capacity(Buffer * const pBD);
char * b_setmark(Buffer * const pBD, short mark);
short b_mark(Buffer * const pBD);
int b_mode(Buffer * const pBD);
size_t b_inc_factor(Buffer * const pBD);
int b_load(FILE * const fi, Buffer * const pBD);
int b_isempty(Buffer * const pBD);
int b_eob(Buffer * const pBD);
char b_getc(Buffer * const pBD);
int b_print(Buffer * const pBD);
Buffer *b_pack(Buffer * const pBD);
char b_rflag(Buffer * const pBD);
short b_retract(Buffer * const pBD);
short b_retract_to_mark(Buffer * const pBD);
short b_getc_offset(Buffer * const pBD);
short b_getaddc_offset(Buffer * const pBD);

#endif

