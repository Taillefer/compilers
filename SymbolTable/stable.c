/* File name: stable.c
*  Compiler:  MS Visual Studio 2012
*  Author:	  Brodie Taillefer, 040 757 711
*  Assignment:Symbol Table #3
*  Date:      March 21, 2015
*  Professor: Sv. Ranev
*  Purpose:  Functions implementing a Symbol Table( Symbol Table Manager (STM) and Symbol Table Database (STDB). Keep track of variables declared and it's attributes (line number, status update, type)
*  Function List: st_create(), st_install(), st_lookup, st_update_type(),st_get_type(),st_update_value(), st_destroy(), st_print(), st_setsize(), set_incoffset(), st_store(), st_sort(), compareASC()
*				  compareDESC()
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>  /* standard library functions and constants */
#include <string.h>  /* string functions */
#include "stable.h" /* Header file for symbol table */

extern STD sym_table; /*extern global sym_table */
/*Static prototypes */
static void st_setsize(void);
static void st_incoffset(void);

/*
* Purpose: Intialize the symbol table and it's database (array of STVR, members, buffer)
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: malloc(),st_setsize(),b_create(),free()
* Parameters: [int st_size, positive value]
* Return Value: Returns std(symbol table database)
*/
STD st_create(int st_size) {
	STD std; /*Create new Symbol table database */
	Buffer *buffer; /*Pointer to buffer struct */
	std.pstvr = (STVR*)malloc(sizeof(STVR)* st_size); /*Malloc dynamic memory for the array of pointers to symbol table records */
	st_setsize(); /*Global function, set the std size to 0 */
	if (!std.pstvr) { /*If we could not malloc space for the array of records */
		return std;
	}
	buffer = b_create(1, 1, 'a'); /*Initialize buffer with max size of 1, this will force us to make sure we have no error's with dangling pointers */
	if (!buffer) { /*If we couldn't create a buffer */
		free(std.pstvr); /* Free the pointer to the records, or else we we have a memory leak */
		return std;
	}
	std.plsBD = buffer; /* Set the database buffer point to the pointer to buffer */
	std.st_offset = DEFAULT_OFFSET; /* Set the offset to 0 */
	std.st_size = st_size; /* Set the size to st_size */
	return std;
}

/*
* Purpose: Used to install (place) a record into the symbol table database.Set's the records attributes.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: st_lookup, b_setmark, b_addc, b_rflag, strlen, st_incoffset
* Parameters: [STD sym_table, valid sym_table (size greater than 0)], [char*lexeme, valid pointer to string], [char type, valid variable type (I,S OR F)],
[int line, valid line number]
* Return Value: Returns R_FAIL_1 (-1) if the sym_table is invalid or invalid type, Returns R_FAIL_2 if unable to add char to sym_table buffer, else returns the offset that the
record was placed at
*/
int st_install(STD sym_table, char *lexeme, char type, int line) {
	int offset; /*Hold the offset where the record was installed */
	int i = 0; /*Counter variable */
	short r_flag = 0; /* Reallocation flag */
	if (sym_table.st_size == ERROR_SIZE) { /*If the sym_table is invalid */
		return R_FAIL_1;
	}
	offset = st_lookup(sym_table, lexeme); /* Lookup to see if the lexeme if already in the database */
	if (offset != R_FAIL_1) { /*If the variable record was found, we shouldn't install it */
		return offset;
	}
	offset = sym_table.st_offset; /*Set the offset to the next free offset in the table */
	if (offset == sym_table.st_size) { /*If the offset is equal to the table size, the database is now full */
		return R_FAIL_1;
	}
	sym_table.pstvr[offset].plex = b_setmark(sym_table.plsBD, b_size(sym_table.plsBD)); /* Set the next record's pointer to lexeme to the buffer's next available char, garbage currently but we will fill in the buffer */
	for (i = 0; i <= (int)strlen(lexeme); i++) { /* Loop through the lexeme */
		if (!b_addc(sym_table.plsBD, lexeme[i])) { /*Attempt to add the char in the lexeme at index i, to the STDB buffer */
			return R_FAIL_2;
		}
		if (b_rflag(sym_table.plsBD) && r_flag == RESET_FLAG) { /*Check to see if the reallocation flag has been set, this means we must re-arrange our lexeme pointers in the database records */
			r_flag = SET_R_FLAG; /*Set our local flag */
		}
	}
	sym_table.pstvr[offset].o_line = line; /*Set the records line number */
	sym_table.pstvr[offset].status_field = STATUS_FIELD_DEFAULT; /*Set the records status_field to the default value, 1111 1111 1111 1000 no update flag or data indicator set */
	if (type == 'I') { /*If the variable belongs to an integer */
		sym_table.pstvr[offset].status_field |= STATUS_FIELD_INT; /*Binary OR the current status field with 0000 0000 0000 0100 to set the data indicator to Integer */
		sym_table.pstvr[offset].i_value.int_val = DEFAULT_INT; /*Set the default value to 0 INT*/
	}
	else if (type == 'F') { /*If the variable belongs to a Float */
		sym_table.pstvr[offset].status_field |= STATUS_FIELD_FP; /* Binary OR the current status field with 0000 0000 0000 0010 to set the data indicator to Float */
		sym_table.pstvr[offset].i_value.fpl_val = DEFAULT_FLOAT; /*Set the default value to 0 FLOAT */
	}
	else if (type == 'S') { /*If the variable belongs to a String */
		sym_table.pstvr[offset].status_field |= STATUS_FIELD_STRING; /*Binary OR the current status field with 0000 0000 0000 0110 to set the data indicator to String */
		sym_table.pstvr[offset].status_field |= STATUS_FIELD_UPDATE; /*Binary OR the current status field to set the update flag */
		sym_table.pstvr[offset].i_value.str_offset = DEFAULT_STRING; /* Set the default value to R_FAIL_1 (-1) */
	}
	else { //Not a valid type, S, I or F
		return R_FAIL_2;
	}
	if (r_flag) { /*If the reallocation flag was set we must re-set our pointers to the correct location or dangling pointers will occur */
		short current_offset = 0; /*Keep track of the current offset from the start of the buffer */
		sym_table.pstvr[0].plex = b_setmark(sym_table.plsBD, 0); /*Set the record at index 0 to the beginning of the buffer */
		for (i = 1; i <= sym_table.st_offset; i++) {
			current_offset += (short)strlen(sym_table.pstvr[i - 1].plex) + 1; /*Increase the current offset by the size of the previous lexeme */
			sym_table.pstvr[i].plex = b_setmark(sym_table.plsBD, current_offset); /*Set the lexeme for record at index i to the buffer+currentoffset */
		}
		r_flag = RESET_FLAG; /*Reset our local re-allocation flag */
	}
	st_incoffset(); /*Global function to increase the database offset */
	return sym_table.st_offset;
}

/*
* Purpose: Search the symbol table database backwards for a record with the desired lexeme
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: strcmp
* Parameters: [STD sym_table, valid table], [char *lexeme, pointer to valid string]
* Return Value: Returns R_FAIL_1 (-1) if the table has an invalid size. Returns -1 if the lexeme was not found, else it returns the offset that it was located at
*/
int st_lookup(STD sym_table, char *lexeme) {
	int offset = R_FAIL_1; /*Set the offset default value to -1 */
	int current_offset; /*Keep track of the offset we are searching at */
	if (sym_table.st_size == ERROR_SIZE) { /*Check to see if we have a valid table */
		return R_FAIL_1;
	}
	current_offset = sym_table.st_offset - 1; /*Set the current offset to search at to the table's next available offset -1 */
	while (current_offset >= ERROR_SIZE) { /*While we are still at a valid offset */
		if (strcmp(lexeme, sym_table.pstvr[current_offset].plex) == 0) { /*string compare the lexeme we are looking for with the lexeme at the record offset */
			offset = current_offset; /*We found the lexeme, we should return out of the loop to stop useless looping */
			return offset;
		}
		current_offset--; /*Else decrement our offset and try the next one */
	}
	return offset;
}

/*
* Purpose: Update the symbol table variable record data type based on the parameter v_type.
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: strcmp
* Parameters: [STD sym_table, valid table], [int vid_offset, within table's size], [char v_type, valid variable type]
* Return Value: Returns R_FAIL_1 (-1) if the table has an invalid size or the records update flag has already been set, else returns the vid_offset
*/
int st_update_type(STD sym_table, int vid_offset, char v_type) {
	short status_field; /*Store the records status_field for bit manipulation */
	if (sym_table.st_offset == ERROR_SIZE) { /*If the tables size invalid */
		return R_FAIL_1;
	}
	status_field = sym_table.pstvr[vid_offset].status_field; /*Set our local status field to the records status field, we don't want to mess with the actually field until we are sure it can be updated */
	if (status_field & STATUS_FIELD_UPDATE) { /*Binary AND with 0000 0000 0000 0001 to see if the first bit is set aka update flag */
		return R_FAIL_1;
	}
	status_field &= STATUS_FIELD_RESET_UPDATE; /* Binary and with 1111 1111 1111 1001 to reset the data indicator and set the update flag */
	if (v_type == 'F') {
		status_field |= STATUS_FIELD_FP; /*Binary or with 0000 0000 0000 0010 to set data indicator to float */
	}
	else if (v_type == 'I') {
		status_field |= STATUS_FIELD_INT; /*Binary or with 0000 0000 0000 0100 to set data indicator to int */
	}
	else { /*Else it was a string or invalid data type */
		return R_FAIL_1;
	}
	status_field |= STATUS_FIELD_UPDATE; /*Binary or with 0000 0000 0000 0001 to set the update flag */
	sym_table.pstvr[vid_offset].status_field = status_field; /*Set the records status_field to our local status_field value after bit manipulation */
	return vid_offset;
}

/*
* Purpose: Update the records data type(InitialValue) at index vid_offset
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: none
* Parameters: [STD sym_table, valid table], [int vid_offset, within table size],[InitialValue i_value, union value]
* Return Value: Returns R_FAIL_1 (-1) if the table has an invalid size, else returns the vid_offset that was updated
*/
int st_update_value(STD sym_table, int vid_offset, InitialValue i_value) {
	if (sym_table.st_size == ERROR_SIZE) { /*Check if the table has valid size */
		return R_FAIL_1;
	}
	sym_table.pstvr[vid_offset].i_value = i_value; /*Set the i_value at record vid_offset to the new i_value union type */
	return vid_offset;
}


/*
* Purpose: Get the data type at the symbol table vid_offset record
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: none
* Parameters: [STD sym_table, valid table], [int vid_offset, within table size]
* Return Value: Returns R_FAIL_1 (-1) if the table has an invalid size. Returns -1 if the lexeme was not found, else it returns the offset that it was located at
*/
char st_get_type(STD sym_table, int vid_offset) {
	unsigned short status_field; /*Local variable to hold the records status_field */
	if (sym_table.st_offset == ERROR_SIZE){ /*Check if the table has valid size */
		return R_FAIL_1;
	}
	status_field = sym_table.pstvr[vid_offset].status_field; /*Set our local status_field to records status_field */
	status_field &= STATUS_FIELD_STRING; /* Binary AND with 0000 0000 0000 0110 will result in leaving just the 2 and 3rd bit set, aka the data type indicator */
	if (status_field == STATUS_FIELD_FP) { /* Is the current status field equal to data indicator of floating point */
		return 'F';
	}
	if (status_field == STATUS_FIELD_INT) { /*Is the current status field equal to data indicator of Int */
		return 'I';
	}
	if (status_field == STATUS_FIELD_STRING) { /*Is the current status field equal to data indicator of String */
		return 'S';
	}
	return R_FAIL_1;
}

/*
* Purpose: Free dynamic memory for the symbol table database
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions:free, b_destroy
* Parameters: [STD sym_table, valid table]
* Return Value: void, no return
*/
void st_destroy(STD sym_table) {
	if (sym_table.pstvr) { /*If the array of pointers is valid, we should free it */
		free(sym_table.pstvr); /*Free pointer's to array of records */
	}
	if (sym_table.plsBD) { /*If the buffer is valid */
		b_destroy(sym_table.plsBD); /* Free the buffer memory */
	}
	st_setsize(); /*Set the table size to 0, invalid table */
}

/*
* Purpose: Print the symbol table record's
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: none
* Parameters: [STD sym_table, valid table]
* Return Value: Returns R_FAIL_1 (-1) if the table has an invalid size, else returns the current table offset
*/
int st_print(STD sym_table) {
	int i = 0; /*Counter variable */
	if (sym_table.st_offset == ERROR_SIZE) { /*Check if the table is valid */
		return R_FAIL_1;
	}
	/*Print the header information */
	printf("\nSymbol Table\n");
	printf("____________\n\n");
	printf("Line Number Variable Identifier\n");
	for (i = 0; i < sym_table.st_offset; i++) { /*Loop through the table records */
		printf("%2d          %s\n", sym_table.pstvr[i].o_line, sym_table.pstvr[i].plex);
	}
	return sym_table.st_offset;
}

/*
* Purpose: Global function, set the table size to 0
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: none
* Parameters: void
* Return Value: no return
*/
static void st_setsize(void) {
	sym_table.st_size = 0;
}

/*
* Purpose: Increment the table offset
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: none
* Parameters: [STD sym_table, valid table]
* Return Value: no return
*/
static void st_incoffset(void) {
	sym_table.st_offset++;
}

/*
* Purpose: Stores the symbol table and its records into a file for later use because the symbol table is currently full
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: fopen,fclose,fprintf,
* Parameters: [STD sym_table, valid table]
* Return Value: Returns R_FAIL_1 (-1) if the table has an invalid size or the File cannot be created, else returns the size of the table
*/
int st_store(STD sym_table) {
	int i = 0; /*Counter variable */
	FILE* file; /*Pointer to file to load */
	char symbol; /*Variable to hold the current record data type */
	char *filename = "$stable.ste"; /*Declare name of our output file */
	if (sym_table.st_size == ERROR_SIZE) { /*Check if the table is valid */
		return R_FAIL_1;
	}
	if ((file = fopen(filename, "w")) == NULL) { /*Attempt to open the file in write state */
		printf("Cannot create file\n"); /*We couldnt write to the file or create it */
		return R_FAIL_1;
	}
	fprintf(file, "%d ", sym_table.st_size); /*Print the size of the table to the file */
	for (i = 0; i<sym_table.st_size; i++) { /*Loop through the table records */
		fprintf(file, "%4X ", sym_table.pstvr[i].status_field); /*Print the status field in hex to the file */
		fprintf(file, "%hu ", strlen(sym_table.pstvr[i].plex)); /*Print the lexeme length to the file */
		fprintf(file, "%s ", sym_table.pstvr[i].plex); /*Print the lexeme to the file */
		fprintf(file, "%d ", sym_table.pstvr[i].o_line); /*Print the line number to the file */
		symbol = st_get_type(sym_table, i); /*Get the current record's data type */
		if (symbol == 'F') { /*If the variable is a float*/
			fprintf(file, "%.2f", sym_table.pstvr[i].i_value.fpl_val); /*Print the record's value union as floating point */
		}
		if (symbol == 'I') { /*If the variable is Integer */
			fprintf(file, "%d", sym_table.pstvr[i].i_value.int_val); /*Print the record's value union as integer */
		}
		if (symbol == 'S') { /*If the variable is String */
			fprintf(file, "%d", sym_table.pstvr[i].i_value.str_offset); /*Print the record's value union's string offset */
		}
		if (i != sym_table.st_offset - 1) { /*If we are not on the last offset, we should print a space */
			fprintf(file, " ");
		}
	}
	fclose(file); /*Close the file */
	printf("Symbol Table stored.\n");
	return sym_table.st_size;
}

/*
* Purpose: Sort the array of record's by the lexeme in either ascending or descending order
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: qsort
* Parameters: [STD sym_table, valid table], [char s_order, A or D]
* Return Value: Returns R_FAIL_1 (-1) if the table has an invalid size or the s_order is invalid, else returns SUCCESS(1)
*/
int st_sort(STD sym_table, char s_order) {
	if (sym_table.st_size == ERROR_SIZE) { /*Check if the table is valid */
		return R_FAIL_1;
	}
	if (s_order == 'A') { /*If we should sort in ascending order */
		qsort(sym_table.pstvr, sym_table.st_offset, sizeof(STVR), compareASC); /*use qsort with compareASC function */
	}
	else if (s_order == 'D') { /*If we should sort in descending order */
		qsort(sym_table.pstvr, sym_table.st_offset, sizeof(STVR), compareDESC); /*use qsort with compareDESC function */
	}
	else { /*s_order is invalid */
		return R_FAIL_1;
	}
	return SUCCESS;
}

/*
* Purpose: Comparator to compare the record's in ascending order. Compares two records with strcmp
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: none
* Parameters: [const void * record1, valid pointer to location], [const void* record2, valid pointer to location]
* Return Value: Returns strcmp of the two pointers
*/
int compareASC(const void* record1, const void* record2) {
	STVR* stvr1 = (STVR*)record1; /*Create pointer to STVR by casting void* to a pointer to STVR */
	STVR* stvr2 = (STVR*)record2; /*Create pointer to STVR by casting void* to a pointer to STVR */
	return strcmp(stvr1->plex, stvr2->plex); /*String compare the two lexemes */
}

/*
* Purpose: Comparator to compare the record's in descending order. Compares two records with strcmp
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 20, 2015
* Called functions: none
* Parameters: [const void * record1, valid pointer to location], [const void* record2, valid pointer to location]
* Return Value: Returns strcmp of the two pointers
*/
int compareDESC(const void* record1, const void* record2) {
	STVR* stvr1 = (STVR*)record1; /*Create pointer to STVR by casting void* to a pointer to STVR */
	STVR* stvr2 = (STVR*)record2; /*Create pointer to STVR by casting void* to a pointer to STVR */
	return strcmp(stvr1->plex, stvr2->plex) * R_FAIL_1; /*String compare the two lexemes and multiply by -1 (reverse ascending ) */
}
