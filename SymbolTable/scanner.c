/* File name: scanner.c
*  Compiler:  MS Visual Studio 2012
*  Author:	  Brodie Taillefer, 040 757 711
*  Assignment:Scanner Assignment #2
*  Date:      March 5, 2015
*  Professor: Sv. Ranev
*  Purpose:  Functions implementing a Lexical Analyzer (scanner). It provides various functions to scan the inputted text for various lexical symbols and patterns
*  Function List: scanner_init(), mlwpar_next_token(), get_next_state(), char_class(), aafunc_02(), aafunc_03(), aafunc_08(), aafunc_05(), aafunc_11(), aafunc_12(),
*				  atool(), isEndLine(), isWhiteSpace(), iskeyword()
*/

/* The #define _CRT_SECURE_NO_WARNINGS should be used in MS Visual Studio projects
* to suppress the warnings about using "unsafe" functions like fopen()
* and standard sting library functions defined in string.h.
* The define does not have any effect in Borland compiler projects.
*/
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>   /* standard input / output */
#include <ctype.h>   /* conversion functions */
#include <stdlib.h>  /* standard library functions and constants */
#include <string.h>  /* string functions */
#include <limits.h>  /* integer types constants */
#include <float.h>   /* floating-point types constants */

/*#define NDEBUG        to suppress assert() call */
#include <assert.h>  /* assert() prototype */

/* project header files */
#include "buffer.h"
#include "token.h"
#include "table.h"
#include "stable.h"

//#define DEBUG  /* for conditional processing */
//#undef  DEBUG

/* Global objects - variables */
/* This buffer is used as a repository for string literals.
It is defined in platy_st.c */
extern Buffer * str_LTBL; /*String literal table */
int line; /* current line number of the source code */
extern int scerrnum;     /* defined in platy_st.c - run-time error number */

/* Local(file) global objects - variables */
static Buffer *lex_buf;/*pointer to temporary lexeme buffer*/
extern STD sym_table;
/* No other global variable declarations/definitiond are allowed */

/* scanner.c static(local) function  prototypes */
static int char_class(char c); /* character class function */
static int get_next_state(int, char, int *); /* state machine function */
static int isKeyword(char * kw_lexeme); /*keywords lookup functuion */
static long atool(char * lexeme); /* converts octal string to decimal value */
static int isWhiteSpace(char c); /*Prototype to check if the char is a whitespace char*/
static int isEndLine(char c); /*Prototype to check if the char is an endline char*/
static Token runtimeErrorToken(); /*Prototype to return error token for runtime error*/

/*
* Purpose: The purpose of scanner_init() is to intialize the scanner and set the mark to the beginning of the buffer
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: b_isempty(), b_setmark(), b_retract_to_mark(), b_reset()
* Parameters: [Pointer to buffer, Must be valid Pointer to buffer (Not Null)]
* Return Value: Returns EXIT_FAILURE(1) if the buffer is empty, EXIT_SUCCESS(0) if properly intialized
*/
int scanner_init(Buffer * sc_buf) {
	if (b_isempty(sc_buf)) return EXIT_FAILURE;/*1*/
	/* in case the buffer has been read previously  */
	b_setmark(sc_buf, 0);
	b_retract_to_mark(sc_buf);
	b_reset(str_LTBL);
	line = 1;
	return EXIT_SUCCESS;/*0*/
	/*   scerrnum = 0;  *//*no need - global ANSI C */
}

/*
* Purpose: The purpose of mlwpar_next_token() is to perform token recognition for each char in the buffer, and return a token tokenized with the correct token code and attribute
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: runtimeErrorToken(), b_setmark(),b_getc_offset(),isEndLine(),b_retract_to_mark(),b_addc(),b_getc(),strcpy(),strncpy(),aafunc_02(),aafunc_03(),aafunc_08(),aafunc_05(),
*					aafunc_11(),aafunc_12()
* Parameters: [pointer to a buffer, buffer must be valid]
* Return Value: Returns the token with with the set code and attribute value based on the lexical analysis
*/
Token mlwpar_next_token(Buffer * sc_buf)
{
	Token t; /* token to return after recognition */
	unsigned char c; /* input symbol */
	int state = 0; /* initial state of the FSM */
	short lexstart;  /*start offset of a lexeme in the input buffer */
	short lexend;    /*end   offset of a lexeme in the input buffer */
	int accept = NOAS; /* type of state - initially not accepting */
	int counter = 0; /* Loop counter*/

	/* If the pointer is invalid, create an error token */
	if (sc_buf == NULL) {
		t = runtimeErrorToken(); //Create an error token
		return t;
	}
	while (1){ /* endless loop broken by token returns it will generate a warning */
		c = b_getc(sc_buf);
		/*Have we reached EOF , create SEOF Token */
		if (c == SEOF || c == '\0') {
			t.code = SEOF_T;
			return t;
		}
		/* Reached ", try to process as a string literal */
		if (c == '"') {
			int strLength; /*Hold the length of the string literal */
			lexstart = (b_getc_offset(sc_buf) - 1); /* Get the start of the string location */
			b_setmark(sc_buf, lexstart); /*Set a mark there so we can retract back */
			do { /*Loop through the buffer getting the char until we reach another quotation mark */
				c = b_getc(sc_buf);
				if (isEndLine(c)) { /* Is the char an end line, if so skip it */
					continue;
				}
				if (c == SEOF || c == '\0') { /*We reached an error, SEOF was found before another quotation mark */
					lexend = b_getc_offset(sc_buf); /*Get the current location */
					strLength = lexend - lexstart; /* Get length of the failed string */
					b_retract_to_mark(sc_buf); /*Retract back to the start of the string */
					while (counter < strLength) { /*Loop through the each char until we have reached the end of the string */
						if (counter == ERR_LEN) { /*If we have reached the ERR_LEN (20) we should break out of the loop */
							break;
						}
						c = b_getc(sc_buf);
						if (strLength <= ERR_LEN || counter < MAX_STR_ERROR) { /* If the str is less than 20, or the counter is currently less than the MAX_STR_ERROR(17) */
							t.attribute.err_lex[counter] = c; /*Add char to the err_lex */
						}
						else{ /*We have reached over the MAX_STR_ERROR add a . */
							t.attribute.err_lex[counter] = '.';
						}
						counter++; /*Increment our loop counter */
					}
					t.attribute.err_lex[counter] = '\0'; /*Add string terminator to stop the buffer from printing garbage */
					counter = 0; /* Reset counter */
					t.code = ERR_T;
					b_setmark(sc_buf, lexend); /*Set a mark to the end of the String to reach SEOF */
					b_retract_to_mark(sc_buf); /*Retract to buffers current mark */
					return t;
				}
			} while (c != '"'); /* Until we reach the next quotation mark */
			lexend = b_getc_offset(sc_buf);
			strLength = lexend - lexstart; /*Find the length of the valid string */
			b_retract_to_mark(sc_buf);
			t.attribute.str_offset = b_getaddc_offset(str_LTBL); /*Set the token attribute to the buffer's addcoffset, start of string */
			while (counter < strLength) { /*Loop through the buffer from start string location until string length */
				counter++;
				c = b_getc(sc_buf);
				if (c == '"') { /*Skip the first quotation mark */
					continue;
				}
				b_addc(str_LTBL, c); /*Add the char to the str literal buffer */
			}
			b_addc(str_LTBL, '\0'); /*Null terminate the buffer string */
			t.code = STR_T; /*Tokenize as string literal */
			return t;
		}
		if (c == '!') { /* If the char is !, it is a comment */
			c = b_getc(sc_buf);
			if (c == '<') { /* If the next char is <, we have a valid comment else its an error */
				c = b_getc(sc_buf);
				while (c != '\0' && c != '\r' && c != SEOF && c != '\n') { /* The comment will skip the entire line or until we reach SEOF */
					c = b_getc(sc_buf);
				}
				if (isEndLine(c)) { /* Increment line if we reach a new line */
					continue;
				}
				t.code = ERR_T;
				strcpy(t.attribute.err_lex, "Missing newline"); /* We are missing a new line feed */
				return t;
			}
			else { /*The comment is an error */
				t.code = ERR_T; /* Tokenize as an error */
				t.attribute.err_lex[0] = '!';
				t.attribute.err_lex[1] = c;
				t.attribute.err_lex[2] = '\0';
				while (c != '\0' && c != '\r' && c != '\n') { /* Ignore the rest of the line or until SEOF */
					c = b_getc(sc_buf);
					if (c == SEOF || c == '\0') { /*If we reach SEOS, retract so we can get it next iteration */
						b_retract(sc_buf);
						return t;
					}
				}
				line++; /*We have reached newline char, increment line counter*/
				return t;
			}
		}
		if (c == '{') { /*If we encounter { tokenize as LBR_T */
			t.code = LBR_T;
			return t;
		}
		if (c == '}') { /*If we encounter } tokenize as RBR_T */
			t.code = RBR_T;
			return t;
		}
		if (c == '(') { /*If we encounter ( tokenize as LPR_T */
			t.code = LPR_T;
			return t;
		}
		if (c == ')') { /*If we encounter ) tokenize as RPR_T*/
			t.code = RPR_T;
			return t;
		}
		if (c == ',') { /*If we encounter , tokenize as COM_T */
			t.code = COM_T;
			return t;
		}
		if (c == '-') { /*If we encounter - tokenize as ART_OP_T.MINUS (1) */
			t.code = ART_OP_T;
			t.attribute.arr_op = MINUS;
			return t;
		}
		if (c == '+') { /*If we encounter + tokenize as ART_OP_T.PLUS (0)*/
			t.code = ART_OP_T;
			t.attribute.arr_op = PLUS;
			return t;
		}
		if (c == '*') { /*If we encounter * tokenize as ART_OP_T.MULT (2) */
			t.code = ART_OP_T;
			t.attribute.arr_op = MULT;
			return t;
		}
		if (c == '/') { /*If we encounter / tokenize as ART_OP_T.DIV (3) */
			t.code = ART_OP_T;
			t.attribute.arr_op = DIV;
			return t;
		}
		if (c == '<'){ /*If we encounter < we have 3 options */
			c = b_getc(sc_buf);
			if (c == '<') { /*If <, we encounter string concat  <<*/
				t.code = SCC_OP_T;
				return t;
			}
			if (c == '>') { /* If we encounter >, we have NE operator <> */
				t.code = REL_OP_T;
				t.attribute.rel_op = NE;
				return t;
			}
			b_retract(sc_buf); //Else we have LT operator REL_OP_T.LT (3)
			t.code = REL_OP_T;
			t.attribute.rel_op = LT;
			return t;
		}
		if (c == ';') { /* IF we encounter ;, tokenize as EOS_T */
			t.code = EOS_T;
			return t;
		}
		if (c == '=') { /*If we encounter =, it can either be Equal operator or assignment operator */
			c = b_getc(sc_buf);
			if (c == '=') { /* It is equal operator , tokenize as REL_OP_T.EQ (0) */
				t.code = REL_OP_T;
				t.attribute.rel_op = EQ;
				return t;
			}
			t.code = ASS_OP_T; /*Else it is assignment Operator, ASS_OP_T */
			b_retract(sc_buf); /*Retract back the char we read in so we can evaluate it */
			return t;
		}
		if (c == '>') { /*If we encounter >, it is the Greater than operator , REL_OP_T.GT(2)  */
			t.code = REL_OP_T;
			t.attribute.rel_op = GT;
			return t;
		}
		if (c == '.') { /*If we encounter a ., we have a logarithmic operator .AND. or .OR. */
			b_setmark(sc_buf, b_getc_offset(sc_buf)); /*Set a mark at the . */
			c = b_getc(sc_buf);
			if (c == 'A' && b_getc(sc_buf) == 'N' && b_getc(sc_buf) == 'D' && b_getc(sc_buf) == '.') { /*If the next 4 chars are AND. , we have log operator AND */
				t.code = LOG_OP_T;
				t.attribute.log_op = AND;
				return t;
			}
			b_retract_to_mark(sc_buf); /*Retract back to the first . */
			c = b_getc(sc_buf);
			if (c == 'O' && b_getc(sc_buf) == 'R' && b_getc(sc_buf) == '.') { /*IF the next 3 chars are OR., we have the log operator OR */
				t.code = LOG_OP_T;
				t.attribute.log_op = OR;
				return t;
			}
			t.code = ERR_T; /*Else we have an error */
			t.attribute.err_lex[0] = '.'; /*Set error token to . */
			t.attribute.err_lex[1] = '\0';
			b_retract_to_mark(sc_buf); /*retract back to the first . */
			return t;
		}

		if (isalnum(c)) { /*If we encounter an alphanumeric or digit, we must use the FSM */
			Buffer *lex_buf; /* Create pointer buffer to intialize later */
			int strLength = 0; /*Keep track the length of the string we wish to process */
			int i = 0; /*Loop Counter variable */
			lexstart = b_getc_offset(sc_buf) - 1; /*Get the start of the string location */
			b_setmark(sc_buf, lexstart); /*Set a mark to the start of the string so we can access it later */
			state = get_next_state(state, c, &accept); /*Get the next state */
			while (accept == NOAS) { /*Get the next char and check for it's state until we reach an accepting state */
				c = (unsigned char)b_getc(sc_buf);
				state = get_next_state(state, c, &accept);
			}
			if (accept == ASWR) { /*If we reach an accepting with retract state, retract the buffer */
				b_retract(sc_buf);
			}
			lexend = b_getc_offset(sc_buf);
			strLength = lexend - lexstart; /*Capture the length of the lexeme */
			lex_buf = b_create((short)(strLength + 2), 0, 'f'); /*Create a buffer with the capacity of the string size + 2 for the string terminator */
			b_retract_to_mark(sc_buf); /*Retract to the start of the string */
			if (!lex_buf) {
				t = runtimeErrorToken();
				return t;
			}
			for (i = 0; i <strLength; i++) { /* Loop through the the string, copying it into our new buffer, lex_buf */
				c = b_getc(sc_buf);
				b_addc(lex_buf, c);
			}
			b_addc(lex_buf, '\0'); /*Null terminate the string */
			t = aa_table[state](b_setmark(lex_buf, 0)); /*Call pointer to function with the desired state, and the buffer lexeme ca_head */
			b_destroy(lex_buf); /*Destory the buffer, freeing dynamic memory */
			return t;
		}
		if (isWhiteSpace(c) || isEndLine(c)) { /*If we reach whitespace or an endline, skip it */
			continue;
		}
		t.code = ERR_T; /*Else we have an error, it didn't fit any of our checks, ie ~ @ */
		t.attribute.err_lex[0] = c;
		t.attribute.err_lex[1] = '\0';
		return t;
	}
}

/*
* Purpose: The purpose of get_next_state() is to get the next state to transition to based on the next char(column is calculated with char_class())
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: char_class()
* Parameters: [int state, valid location in table], [char c, any ascii char], [int *accept, Valid pointer to int]
* Return Value: Return next location to transition to in the table.
*/
int get_next_state(int state, char c, int *accept) {
	int col;
	int next;
	col = char_class(c); /*Get what column the charachter is from */
	next = st_table[state][col]; /*Use the finite table to grab the row (state) that corresponds to the state, and it's index at col */

#ifdef DEBUG
	printf("Input symbol: %c Row: %d Column: %d Next: %d \n", c, state, col, next);
#endif
	/*
	The assert(int test) macro can be used to add run-time diagnostic to programs
	and to "defend" from producing unexpected results.
	assert() is a macro that expands to an if statement;
	if test evaluates to false (zero) , assert aborts the program
	(by calling abort()) and sends the following message on stderr:

	Assertion failed: test, file filename, line linenum

	The filename and linenum listed in the message are the source file name
	and line number where the assert macro appears.
	If you place the #define NDEBUG directive ("no debugging")
	in the source code before the #include <assert.h> directive,
	the effect is to comment out the assert statement.
	*/
	assert(next != IS);

	/*
	The other way to include diagnostics in a program is to use
	conditional preprocessing as shown bellow. It allows the programmer
	to send more details describing the run-time problem.
	Once the program is tested thoroughly #define DEBUG is commented out
	or #undef DEBUF is used - see the top of the file.
	*/
#ifdef DEBUG
	if (next == IS){
		printf("Scanner Error: Illegal state:\n");
		printf("Input symbol: %c Row: %d Column: %d\n", c, state, col);
		exit(1);
	}
#endif
	*accept = as_table[next];
	return next;
}

/*
* Purpose: The purpose of char_class() is to take the inputted char and calculate what column it belongs to, alpa = 0, digit 0 = 1 , 1-7 = 2, 8-9 = 3
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: isalpha(), isdigit()
* Parameters: [char c, valid ascii char]
* Return Value: Return column the char is located in.
*/
int char_class(char c) {
	/* Check if c is an alphanumeric char */
	if (isalpha(c)) {
		return 0;
	}
	/* Check if c is a digit 0-9 */
	if (isdigit(c)) {
		/* Get the integer value of the char by subtracting the char by the ascii value 0 */
		int digit = (int)c - '0';
		/*  is the digit 0? */
		if (digit == 0) {
			return 1;
		}
		/* Is it an octal digit */
		if (digit > 0 && digit < 8) {
			return 2;
		}
		/* It's a 8 or 9 */
		return 3;
	}
	/*Is  it . */
	if (c == '.') {
		return 4;
	}
	/* Is it # */
	if (c == '#') {
		return 5;
	}
	/* If it's anything else */
	return 6;
}

/*
* Purpose: Accepting function for the AVID/KEYWORD
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: strlen(), strncpy(), strcpy()
* Parameters: [char lexeme[],array with valid string]
* Return Value: Return Token with either KW_T or AVID_T
*/
Token aa_func02(char lexeme[]){
	Token t;
	char *tempLexeme = 0;
	int offset = 0;
	int index = isKeyword(lexeme); /*Check to see if the lexeme is a valid keyword */
	if (index != -1) { /*If it is a keyword, assign keyword token */
		t.code = KW_T; /*Tokenize as Keyword token */
		t.attribute.kwt_idx = index; /*With the keyword array index  */
		return t;
	}
	/*Else we have a AVID Token */
	t.code = AVID_T;
	if (strlen(lexeme) > VID_LEN) { /*If the string length is greater than the VID_LEN , we must only copy upto VID_LEN */
		tempLexeme = (char *)malloc(sizeof(char) * VID_LEN + 1); /*Allocate memory to store the shortened lexeme */
		strncpy(tempLexeme, lexeme, VID_LEN);
		tempLexeme[VID_LEN] = '\0';
		lexeme = tempLexeme;
	}
	/*Check if the AVID is a Integer variable, begins with i,o,d or n */
	if (lexeme[0] == 'i' || lexeme[0] == 'o' || lexeme[0] == 'd' || lexeme[0] == 'n') {
		offset = st_install(sym_table, lexeme, 'I', line);  /*Install the AVID in the database as integer */
	}
	else { /*Else the AVID is a floating point variable */
		offset = st_install(sym_table, lexeme, 'F', line); /*Install the AVID in the database as Floating point */
	}
	if (tempLexeme) { /* If the shortend lexeme was used, we should free it */
		free(tempLexeme);
	}
	if (offset == R_FAIL_1) { /*If AVID couldn't be installed in the table, the table is full */
		printf("\nError: The Symbol Table is full - install failed.\n\n");
		st_store(sym_table); /*Store the symbol table and it's records in a file for later use */
		exit(EXIT_FAILURE);
	}
	t.attribute.vid_offset = offset; /*Set the vid_offset to the variables record index */
	return t;
}

/*
* Purpose: Accepting function for SVID
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: strcpy()e
* Parameters: [char lexeme[], valid string SVID ]
* Return Value: Return token tokenized as SVID
*/
Token aa_func03(char lexeme[]){
	Token t;
	int offset = 0; /*Hold the offset of install offset */
	char *tempLexeme = 0; /*Hold the shortened lexeme */
	t.code = SVID_T; /*It is a SVID */
	if (strlen(lexeme) > VID_LEN) { /*If the lexeme is greater than VID_LEN we must copy over char by char */
		tempLexeme = (char *)malloc(sizeof(char) * VID_LEN + 1); /*Allocate memory to store the shortened lexeme */
		strncpy(tempLexeme, lexeme, VID_LEN); /*Copy the shortened lexeme into the temporary lexeme */
		tempLexeme[VID_LEN] = '\0';
		lexeme = tempLexeme;
	}
	offset = st_install(sym_table, lexeme, 'S', line); /*Attempt to install the SVID into the table */
	if (offset == R_FAIL_1) { /*If we couldn't install it into the table, the table is full */
		printf("\nError: The Symbol Table is full - install failed.\n\n");
		st_store(sym_table); /*Store the symbol table and it's records in a file for later use */
		exit(EXIT_FAILURE); /*Exit the program */
	}
	t.attribute.vid_offset = offset; /*Set the vid_offset to the variables record index */
	return t;
}

/*
* Purpose: Accepting function for FPL
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: strncpy,strod()
* Parameters: [char lexeme[],valid string)
* Return Value: Return token tokenized as FPL, or ERR_T if FPL is out of range
*/
Token aa_func08(char lexeme[]){
	Token t;
	double value = strtod(lexeme, NULL); /*Convert the lexeme as a floating point, and returns a double */
	if (value > FLT_MAX || value < 0.0f || (value < FLT_MIN && value != 0.0f)) { /*If the double value is out of range of our Platypus FLT value , Less than float 0, or greater than FLT_MAX */
		t.code = ERR_T; /*If not error, call the error accepting function to create ERR_T */
		strncpy(t.attribute.err_lex, lexeme, ERR_LEN);
		t.attribute.err_lex[ERR_LEN] = '\0';
		return t;
	}
	t.code = FPL_T; /*Else we can tokenize as FPL */
	t.attribute.flt_value = (float)value; /*Cast the value as a float, and set the token attribute*/
	return t;
}

/*
* Purpose: Accepting function for DIL/0 (INL_T)
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: atol(),strlen()
* Parameters: [char lexeme[],valid string)
* Return Value: Return token tokenized as INL_T or ERR_T
*/
Token aa_func05(char lexeme[]){
	Token t;
	double value = atol(lexeme); /* Convert the lexeme as long, returning a double*/
	int strLength = strlen(lexeme); /*Get the length of the string */
	if (value < MIN_INT || strLength > INL_LEN || value > PLATYPUS_MAX) { /*Check to see if the converted double is within platypus ranges */
		t.code = ERR_T; /*If not error, call the error accepting function to create ERR_T */
		strncpy(t.attribute.err_lex, lexeme, ERR_LEN);
		t.attribute.err_lex[ERR_LEN] = '\0';
		return t;
	}
	t.code = INL_T; /*Else we tokenize as INL_T */
	t.attribute.int_value = (int)value;
	return t;
}

/*
* Purpose: Accepting function for OIL(IL)
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: atool(),strncpy()
* Parameters: [char lexeme[],valid string)
* Return Value: Return token tokenized as INL_T, or ERR_T
*/
Token aa_func11(char lexeme[]){
	Token t;
	long value = atool(lexeme); /*Use our function to convert lexeme to int representation */
	if (value < INT_MIN || value > PLATYPUS_MAX) { /*Check to see if it is within our octal range */
		t.code = ERR_T; /*If not error, call the error accepting function to create ERR_T */
		strncpy(t.attribute.err_lex, lexeme, ERR_LEN);
		t.attribute.err_lex[ERR_LEN] = '\0';
		return t;
	}
	t.code = INL_T; /*Tokenize as INL_T */
	t.attribute.int_value = (int)value;
	return t;
}

/*
* Purpose: Accepting function for ERROR Token
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: strcpy(), strncpy()
* Parameters: [char lexeme[],valid string)
* Return Value: Return token as ERR_T
*/
Token aa_func12(char lexeme[]){
	Token t;
	t.code = ERR_T; /*Tokenize as an error */
	if (strlen(lexeme) > ERR_LEN) { /*If the lexeme is larger than ERR_LEN */
		strncpy(t.attribute.err_lex, lexeme, ERR_LEN); /*Copy in the first 20 chars */
		t.attribute.err_lex[ERR_LEN] = '\0'; /*Null terminate */
		return t;
	}
	strcpy(t.attribute.err_lex, lexeme); /*Else copy in all the chars */
	t.attribute.err_lex[ERR_LEN] = '\0';
	return t;
}

/*
* Purpose: Used to convert octal literal string to int literal
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: strtol()
* Parameters: [pointer to lexeme, valid pointer)
* Return Value: Return long value of the octal literal
*/
long atool(char * lexeme){
	long value;
	if (lexeme) { /*If we have a valid pointer to the lexeme */
		value = strtol(lexeme, NULL, 8); /*Convert octal string to int representation */
		return value;
	}
	return FAIL; /*Not valid pointer return FAIL(0) */
}

/*
* Purpose: Checks to see if the char is an endline char
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: none
* Parameters: [char c, valid ascii char)
* Return Value: Return SUCCESS(1) if the char is an endline char, FAIL(0) if not
*/
int isEndLine(char c){
	if (c == '\n' || c == '\r'){ /*Is the char an endline char */
		line++; /*Increment line counter */
		return SUCCESS;
	}
	return FAIL; /*Isn't an endline */
}

/*
* Purpose: Checks to see if lexeme is a keyword in the kw_table
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: strcmp()
* Parameters: [char c, valid ascii char]
* Return Value: Return i, the index in the kw_table where the keyword exists, or R_FAIL_1(-1) if it's an invalid pointer or not a keyword
*/
int isKeyword(char * kw_lexeme) {
	int i = 0;
	if (!kw_lexeme) { /*Do we have a valid pointer ? */
		return R_FAIL_1;
	}
	for (i = 0; i < KWT_SIZE; i++) { /*Search for every index in the kw_table */
		if (!strcmp(kw_lexeme, kw_table[i])) { /*If the lexeme == kw_table at index i , we have a keyword */
			return i;
		}
	}
	return R_FAIL_1; /*Else no match */
}

/*
* Purpose: Checks to see if the char is whitespace
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: none
* Parameters: [char c, valid ascii char]
* Return Value: return SUCCESS(1) if is a whitespace, FAIL(0) if not
*/
int isWhiteSpace(char c) {
	return (c == ' ' || c == '\t' || c == '\f' || c == '\v' || c == '\r') ? SUCCESS : FAIL; /*If we have a white space char, return SUCCESS(1) else FAIL(0)*/
}

/*
* Purpose: Creates a runtime error token with the attribute, "RUNTIME ERROR:"
* Author:  Brodie Taillefer
* History/Revisions: V1.0 March 5, 2015
* Called functions: strcpy()
* Parameters: [none]
* Return Value: return token tokenized as ERR_T
*/
Token runtimeErrorToken() {
	Token t;
	scerrnum = 5; /*Se the scerrnum to a non zero value */
	t.code = ERR_T; /*Tokenize as erro */
	strcpy(t.attribute.err_lex, "RUNTIME ERROR:");
	t.attribute.err_lex[ERR_LEN] = '\0';
	return t;
}


